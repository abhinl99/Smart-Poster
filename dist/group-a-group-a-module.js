(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["group-a-group-a-module"],{

/***/ "./node_modules/@qontu/ngx-inline-editor/ngx-inline-editor.es5.js":
/*!************************************************************************!*\
  !*** ./node_modules/@qontu/ngx-inline-editor/ngx-inline-editor.es5.js ***!
  \************************************************************************/
/*! exports provided: InlineEditorModule, InlineEditorComponent, InputTextComponent, InputDateComponent, InputDatetimeComponent, InputNumberComponent, InputRangeComponent, InputPasswordComponent, InputSelectComponent, InputTextareaComponent, InputTimeComponent, InputCheckboxComponent, InputBase */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InlineEditorModule", function() { return InlineEditorModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InlineEditorComponent", function() { return InlineEditorComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InputTextComponent", function() { return InputTextComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InputDateComponent", function() { return InputDateComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InputDatetimeComponent", function() { return InputDatetimeComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InputNumberComponent", function() { return InputNumberComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InputRangeComponent", function() { return InputRangeComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InputPasswordComponent", function() { return InputPasswordComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InputSelectComponent", function() { return InputSelectComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InputTextareaComponent", function() { return InputTextareaComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InputTimeComponent", function() { return InputTimeComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InputCheckboxComponent", function() { return InputCheckboxComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InputBase", function() { return InputBase; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();



var InlineEditorService = (function () {
    /**
     * @param {?} events
     * @param {?=} config
     */
    function InlineEditorService(events, config) {
        var _this = this;
        this.events = events;
        this.config = config;
        this.onUpdateStateOfService = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.subscriptions = {};
        this.subscriptions.onUpdateStateSubscription = this.onUpdateStateOfService.subscribe(function (state) { return _this.state = state; });
    }
    /**
     * @param {?} config
     * @return {?}
     */
    InlineEditorService.prototype.setConfig = function (config) {
        this.config = config;
    };
    /**
     * @return {?}
     */
    InlineEditorService.prototype.getConfig = function () {
        return this.config;
    };
    /**
     * @return {?}
     */
    InlineEditorService.prototype.getState = function () {
        return this.state.clone();
    };
    /**
     * @return {?}
     */
    InlineEditorService.prototype.destroy = function () {
        Object.values(this.subscriptions).forEach(function (subscription) { return subscription.unsubscribe(); });
    };
    return InlineEditorService;
}());
var InputBase = (function () {
    /**
     * @param {?} injector
     */
    function InputBase(injector) {
        var _this = this;
        this.injector = injector;
        this.isNumeric = false;
        this.isRegexTestable = false;
        this.isLengthTestable = false;
        this.subscriptions = {};
        this.renderer = injector.get(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer"]);
        this.service = injector.get(InlineEditorService);
        this.cd = injector.get(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]);
        this.onUpdateConfig(this.service.getConfig());
        this.state = this.service.getState().clone();
        this.subscriptions.onUpdateConfigSubcription = this.service.events.internal.onUpdateConfig.subscribe(function (config) { return _this.onUpdateConfig(config); });
        this.subscriptions.onUpdateStateSubscription = this.service.events.internal.onUpdateStateOfChild.subscribe(function (state) {
            var newState = state.getState();
            _this.updateState(_this.state.newState(Object.assign({}, newState, { empty: _this.isEmpty(newState.value) })));
            _this.service.events.internal.onUpdateStateOfParent.emit(_this.state.clone());
        });
    }
    Object.defineProperty(InputBase.prototype, "value", {
        /**
         * @return {?}
         */
        get: function () {
            return this.state.getState().value;
        },
        /**
         * @param {?} value
         * @return {?}
         */
        set: function (value) {
            if (this.value === value) {
                return;
            }
            this.updateState(this.state.newState(Object.assign({}, this.state.getState(), { empty: this.isEmpty(value), value: value })));
            this.service.events.internal.onChange.emit({
                state: this.state.clone(),
            });
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    InputBase.prototype.ngOnChanges = function () { };
    /**
     * @return {?}
     */
    InputBase.prototype.ngOnInit = function () {
        this.inputElement = this.inputRef.nativeElement;
    };
    /**
     * @return {?}
     */
    InputBase.prototype.ngDoCheck = function () { };
    /**
     * @return {?}
     */
    InputBase.prototype.ngAfterContentInit = function () { };
    /**
     * @return {?}
     */
    InputBase.prototype.ngAfterContentChecked = function () { };
    /**
     * @return {?}
     */
    InputBase.prototype.ngAfterViewInit = function () { };
    /**
     * @return {?}
     */
    InputBase.prototype.ngAfterViewChecked = function () { };
    /**
     * @return {?}
     */
    InputBase.prototype.ngOnDestroy = function () {
        Object.values(this.subscriptions).forEach(function (subscription) { return subscription.unsubscribe(); });
    };
    /**
     * @param {?} newConfig
     * @return {?}
     */
    InputBase.prototype.onUpdateConfig = function (newConfig) {
        this.config = newConfig;
    };
    /**
     * @return {?}
     */
    InputBase.prototype.save = function () {
        this.service.events.internal.onSave.emit({
            state: this.state.clone(),
        });
    };
    /**
     * @return {?}
     */
    InputBase.prototype.cancel = function () {
        this.service.events.internal.onCancel.emit({
            state: this.state.clone(),
        });
    };
    /**
     * @param {?} event
     * @return {?}
     */
    InputBase.prototype.onEnter = function (event) {
        this.service.events.internal.onEnter.emit({
            event: event,
            state: this.state.clone(),
        });
    };
    /**
     * @param {?} event
     * @return {?}
     */
    InputBase.prototype.onEscape = function (event) {
        this.service.events.internal.onEscape.emit({
            event: event,
            state: this.state.clone(),
        });
    };
    /**
     * @param {?} event
     * @return {?}
     */
    InputBase.prototype.onBlur = function (event) {
        this.service.events.internal.onBlur.emit({
            event: event,
            state: this.state.clone(),
        });
    };
    /**
     * @param {?} event
     * @return {?}
     */
    InputBase.prototype.onClick = function (event) {
        this.service.events.internal.onClick.emit({
            event: event,
            state: this.state.clone(),
        });
    };
    /**
     * @param {?} event
     * @return {?}
     */
    InputBase.prototype.onKeyPress = function (event) {
        this.service.events.internal.onKeyPress.emit({
            event: event,
            state: this.state.clone(),
        });
    };
    /**
     * @param {?} event
     * @return {?}
     */
    InputBase.prototype.onFocus = function (event) {
        this.service.events.internal.onFocus.emit({
            event: event,
            state: this.state.clone(),
        });
    };
    /**
     * @return {?}
     */
    InputBase.prototype.checkValue = function () {
        var /** @type {?} */ errs = [];
        var value = this.state.getState().value;
        if (this.canTestRegex(this.config)) {
            if (!new RegExp(/** @type {?} */ (this.config.pattern)).test(value != null && value !== false ? value : '')) {
                errs.push({
                    type: "PATTERN_ERROR",
                    message: "Test pattern has failed",
                });
            }
        }
        if (this.canTestLength(this.config)) {
            var _a = this.config, min = _a.min, max = _a.max;
            var /** @type {?} */ length = value ? (this.isNumeric ? Number(value) : value.length) : 0;
            if (length < min || length > max) {
                errs.push({
                    type: "LENGTH_ERROR",
                    message: "Test length has failed",
                });
            }
        }
        return errs;
    };
    /**
     * @return {?}
     */
    InputBase.prototype.showText = function () {
        return this.state.isEmpty() ? this.config.empty : this.state.getState().value;
    };
    /**
     * @return {?}
     */
    InputBase.prototype.focus = function () {
        var _this = this;
        setTimeout(function () { return _this.renderer.invokeElementMethod(_this.inputElement, "focus", []); });
    };
    /**
     * @return {?}
     */
    InputBase.prototype.select = function () {
        var _this = this;
        setTimeout(function () { return _this.renderer.invokeElementMethod(_this.inputElement, "select", []); });
    };
    /**
     * @param {?} newState
     * @return {?}
     */
    InputBase.prototype.updateState = function (newState) {
        var _a = this.state.getState(), wasEmpty = _a.empty, wasDisabled = _a.disabled;
        if (newState.isEmpty() && newState.isEmpty() !== wasEmpty) {
            // onEmpty()
        }
        if (newState.isDisabled() && newState.isDisabled() !== wasDisabled) {
            // onDisabled()
        }
        this.state = newState;
        this.cd.markForCheck();
        this.service.onUpdateStateOfService.emit(this.state.clone());
    };
    /**
     * @param {?} value
     * @return {?}
     */
    InputBase.prototype.isEmpty = function (value) {
        return value == null || value === "";
    };
    /**
     * @param {?} config
     * @return {?}
     */
    InputBase.prototype.canTestRegex = function (config) {
        return this.isRegexTestable &&
            config.pattern != null &&
            (config.pattern instanceof RegExp || typeof config.pattern === "string");
    };
    /**
     * @param {?} config
     * @return {?}
     */
    InputBase.prototype.canTestLength = function (config) {
        return (this.isNumeric || this.isLengthTestable) &&
            (config.min != null || config.max != null);
    };
    return InputBase;
}());
InputBase.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                template: " ",
                changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush,
            },] },
];
/**
 * @nocollapse
 */
InputBase.ctorParameters = function () { return [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"], },
]; };
InputBase.propDecorators = {
    'inputRef': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"], args: ["inputRef",] },],
};
var InputNumberComponent = (function (_super) {
    __extends(InputNumberComponent, _super);
    /**
     * @param {?} injector
     */
    function InputNumberComponent(injector) {
        var _this = _super.call(this, injector) || this;
        _this.isNumeric = true;
        return _this;
    }
    return InputNumberComponent;
}(InputBase));
InputNumberComponent.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                selector: "inline-editor-number",
                styles: ["a {     text-decoration: none;     color: #428bca;     border-bottom: dashed 1px #428bca;     cursor: pointer;     line-height: 2;     margin-right: 5px;     margin-left: 5px; }   /* editable-empty */  .editable-empty, .editable-empty:hover, .editable-empty:focus, a.editable-empty, a.editable-empty:hover, a.editable-empty:focus {     font-style: italic;     color: #DD1144;     text-decoration: none; }  .inlineEditForm {     display: inline-block;     white-space: nowrap;     margin: 0; }  #inlineEditWrapper {     display: inline-block; }  .inlineEditForm input, select {     width: auto;     display: inline; }  .editInvalid {     color: #a94442;     margin-bottom: 0; }  .error {     border-color: #a94442; }  [hidden] {     display: none; }"],
                template: "<input #inputRef type=\"number\" class=\"form-control\" (keyup.enter)=\"onEnter($event)\"\n                (keyup.escape)=\"onEscape($event)\" (focus)=\"onFocus($event)\" (blur)=\"onBlur($event)\" (blur)=\"onBlur($event)\"\n                (keypress)=\"onKeyPress($event)\" [(ngModel)]=\"value\" [required]=\"config.required\"\n                [disabled]=\"state.isDisabled()\" [name]=\"config.name\" [placeholder]=\"config.placeholder\"\n                [size]=\"config.size\"/>",
                changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush,
            },] },
];
/**
 * @nocollapse
 */
InputNumberComponent.ctorParameters = function () { return [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"], },
]; };
var InputTextComponent = (function (_super) {
    __extends(InputTextComponent, _super);
    /**
     * @param {?} injector
     */
    function InputTextComponent(injector) {
        var _this = _super.call(this, injector) || this;
        _this.isRegexTestable = true;
        _this.isLengthTestable = true;
        return _this;
    }
    return InputTextComponent;
}(InputBase));
InputTextComponent.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                selector: "inline-editor-text",
                styles: ["a {     text-decoration: none;     color: #428bca;     border-bottom: dashed 1px #428bca;     cursor: pointer;     line-height: 2;     margin-right: 5px;     margin-left: 5px; }   /* editable-empty */  .editable-empty, .editable-empty:hover, .editable-empty:focus, a.editable-empty, a.editable-empty:hover, a.editable-empty:focus {     font-style: italic;     color: #DD1144;     text-decoration: none; }  .inlineEditForm {     display: inline-block;     white-space: nowrap;     margin: 0; }  #inlineEditWrapper {     display: inline-block; }  .inlineEditForm input, select {     width: auto;     display: inline; }  .editInvalid {     color: #a94442;     margin-bottom: 0; }  .error {     border-color: #a94442; }  [hidden] {     display: none; }"],
                template: "<input #inputRef type=\"text\" (keyup.enter)=\"onEnter($event)\" (keyup.escape)=\"onEscape($event)\"\n                (focus)=\"onFocus($event)\" (blur)=\"onBlur($event)\" (click)=\"onClick($event)\" (keypress)=\"onKeyPress($event)\"\n                class=\"form-control\" [(ngModel)]=\"value\" [required]=\"config.required\"\n                [disabled]=\"state.isDisabled()\" [name]=\"config.name\" [placeholder]=\"config.placeholder\"\n                [size]=\"config.size\"/>",
                changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush,
            },] },
];
/**
 * @nocollapse
 */
InputTextComponent.ctorParameters = function () { return [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"], },
]; };
var InputPasswordComponent = (function (_super) {
    __extends(InputPasswordComponent, _super);
    /**
     * @param {?} injector
     */
    function InputPasswordComponent(injector) {
        var _this = _super.call(this, injector) || this;
        _this.isRegexTestable = true;
        _this.isLengthTestable = true;
        return _this;
    }
    /**
     * @return {?}
     */
    InputPasswordComponent.prototype.showText = function () {
        var /** @type {?} */ isEmpty = this.state.isEmpty();
        var /** @type {?} */ value = String(this.state.getState().value);
        return isEmpty ?
            this.config.empty :
            "*".repeat(value.length);
    };
    return InputPasswordComponent;
}(InputBase));
InputPasswordComponent.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                selector: "inline-editor-password",
                styles: ["a {     text-decoration: none;     color: #428bca;     border-bottom: dashed 1px #428bca;     cursor: pointer;     line-height: 2;     margin-right: 5px;     margin-left: 5px; }   /* editable-empty */  .editable-empty, .editable-empty:hover, .editable-empty:focus, a.editable-empty, a.editable-empty:hover, a.editable-empty:focus {     font-style: italic;     color: #DD1144;     text-decoration: none; }  .inlineEditForm {     display: inline-block;     white-space: nowrap;     margin: 0; }  #inlineEditWrapper {     display: inline-block; }  .inlineEditForm input, select {     width: auto;     display: inline; }  .editInvalid {     color: #a94442;     margin-bottom: 0; }  .error {     border-color: #a94442; }  [hidden] {     display: none; }"],
                template: "<input #inputRef type=\"password\" class=\"form-control\" (keyup.enter)=\"onEnter($event)\"\n                (keyup.escape)=\"onEscape($event)\" (focus)=\"onFocus($event)\" (blur)=\"onBlur($event)\" (click)=\"onClick($event)\"\n                (keypress)=\"onKeyPress($event)\" [(ngModel)]=\"value\" [required]=\"config.required\"\n                [disabled]=\"state.isDisabled()\" [name]=\"config.name\" [placeholder]=\"config.placeholder\"\n                [size]=\"config.size\"/>",
                changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush,
            },] },
];
/**
 * @nocollapse
 */
InputPasswordComponent.ctorParameters = function () { return [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"], },
]; };
var InputRangeComponent = (function (_super) {
    __extends(InputRangeComponent, _super);
    /**
     * @param {?} injector
     */
    function InputRangeComponent(injector) {
        var _this = _super.call(this, injector) || this;
        _this.isNumeric = true;
        return _this;
    }
    return InputRangeComponent;
}(InputBase));
InputRangeComponent.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                selector: "inline-editor-range",
                styles: ["a {     text-decoration: none;     color: #428bca;     border-bottom: dashed 1px #428bca;     cursor: pointer;     line-height: 2;     margin-right: 5px;     margin-left: 5px; }   /* editable-empty */  .editable-empty, .editable-empty:hover, .editable-empty:focus, a.editable-empty, a.editable-empty:hover, a.editable-empty:focus {     font-style: italic;     color: #DD1144;     text-decoration: none; }  .inlineEditForm {     display: inline-block;     white-space: nowrap;     margin: 0; }  #inlineEditWrapper {     display: inline-block; }  .inlineEditForm input, select {     width: auto;     display: inline; }  .editInvalid {     color: #a94442;     margin-bottom: 0; }  .error {     border-color: #a94442; }  [hidden] {     display: none; }"],
                template: "<input #inputRef type=\"range\" class=\"form-control\" (keyup.enter)=\"onEnter($event)\"\n                (keyup.escape)=\"onEscape($event)\" (focus)=\"onFocus($event)\" (blur)=\"onBlur($event)\" (click)=\"onClick($event)\"\n                (keypress)=\"onKeyPress($event)\" [(ngModel)]=\"value\" [required]=\"config.required\"\n                [disabled]=\"state.isDisabled()\" [name]=\"config.name\" [placeholder]=\"config.placeholder\"\n                [min]=\"config.min\" [max]=\"config.max\"/>",
                changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush,
            },] },
];
/**
 * @nocollapse
 */
InputRangeComponent.ctorParameters = function () { return [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"], },
]; };
var InputCheckboxComponent = (function (_super) {
    __extends(InputCheckboxComponent, _super);
    /**
     * @param {?} injector
     */
    function InputCheckboxComponent(injector) {
        return _super.call(this, injector) || this;
    }
    /**
     * @return {?}
     */
    InputCheckboxComponent.prototype.showText = function () {
        return this.value ? this.config.checkedText : this.config.uncheckedText;
    };
    return InputCheckboxComponent;
}(InputBase));
InputCheckboxComponent.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                selector: "inline-editor-checkbox",
                styles: ["a {     text-decoration: none;     color: #428bca;     border-bottom: dashed 1px #428bca;     cursor: pointer;     line-height: 2;     margin-right: 5px;     margin-left: 5px; }   /* editable-empty */  .editable-empty, .editable-empty:hover, .editable-empty:focus, a.editable-empty, a.editable-empty:hover, a.editable-empty:focus {     font-style: italic;     color: #DD1144;     text-decoration: none; }  .inlineEditForm {     display: inline-block;     white-space: nowrap;     margin: 0; }  #inlineEditWrapper {     display: inline-block; }  .inlineEditForm input, select {     width: auto;     display: inline; }  .editInvalid {     color: #a94442;     margin-bottom: 0; }  .error {     border-color: #a94442; }  [hidden] {     display: none; }"],
                template: "<input #inputRef type=\"checkbox\" class=\"form-control\" (focus)=\"onFocus($event)\" (blur)=\"onBlur($event)\"\n                (keypress)=\"onKeyPress($event)\" [(ngModel)]=\"value\" [required]=\"config.required\"\n                [disabled]=\"state.isDisabled()\" [name]=\"config.name\"/>",
                changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush,
            },] },
];
/**
 * @nocollapse
 */
InputCheckboxComponent.ctorParameters = function () { return [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"], },
]; };
var InputTextareaComponent = (function (_super) {
    __extends(InputTextareaComponent, _super);
    /**
     * @param {?} injector
     */
    function InputTextareaComponent(injector) {
        var _this = _super.call(this, injector) || this;
        _this.isRegexTestable = true;
        _this.isLengthTestable = true;
        return _this;
    }
    return InputTextareaComponent;
}(InputBase));
InputTextareaComponent.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                selector: "inline-editor-textarea",
                styles: ["a {     text-decoration: none;     color: #428bca;     border-bottom: dashed 1px #428bca;     cursor: pointer;     line-height: 2;     margin-right: 5px;     margin-left: 5px; }   /* editable-empty */  .editable-empty, .editable-empty:hover, .editable-empty:focus, a.editable-empty, a.editable-empty:hover, a.editable-empty:focus {     font-style: italic;     color: #DD1144;     text-decoration: none; }  .inlineEditForm {     display: inline-block;     white-space: nowrap;     margin: 0; }  #inlineEditWrapper {     display: inline-block; }  .inlineEditForm input, select {     width: auto;     display: inline; }  .editInvalid {     color: #a94442;     margin-bottom: 0; }  .error {     border-color: #a94442; }  [hidden] {     display: none; }"],
                template: "<textarea #inputRef class=\"form-control\" (keyup.enter)=\"onEnter($event)\"\n                (keyup.escape)=\"onEscape($event)\" (focus)=\"onFocus($event)\" (blur)=\"onBlur($event)\" (click)=\"onClick($event)\"\n                (keypress)=\"onKeyPress($event)\" [(ngModel)]=\"value\" [required]=\"config.required\"\n                [rows]=\"config.rows\" [cols]=\"config.cols\" [disabled]=\"state.isDisabled()\" [name]=\"config.name\"\n                [placeholder]=\"config.placeholder\"></textarea>",
                changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush,
            },] },
];
/**
 * @nocollapse
 */
InputTextareaComponent.ctorParameters = function () { return [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"], },
]; };
var InputSelectComponent = (function (_super) {
    __extends(InputSelectComponent, _super);
    /**
     * @param {?} injector
     */
    function InputSelectComponent(injector) {
        var _this = _super.call(this, injector) || this;
        _this.subscriptions.onUpdateConfigSubcription.unsubscribe();
        _this.subscriptions.onUpdateConfigSubcription = _this.service.events.internal.onUpdateConfig.subscribe(function (config) { return _this.onUpdateConfig(config); });
        return _this;
    }
    /**
     * @param {?} config
     * @return {?}
     */
    InputSelectComponent.prototype.onUpdateConfig = function (config) {
        _super.prototype.onUpdateConfig.call(this, config);
        var options = this.config.options;
        this.config.options = options instanceof Array ?
            {
                data: options,
                value: "value",
                text: "text",
            } : options;
        this.config = Object.assign({}, this.config);
    };
    /**
     * @return {?}
     */
    InputSelectComponent.prototype.showText = function () {
        var _a = this.config.options, keyOfText = _a.text, keyOfValue = _a.value, options = _a.data;
        var /** @type {?} */ currentValue = this.state.getState().value;
        var /** @type {?} */ optionSelected = this.getOptionSelected(currentValue, keyOfValue, options);
        return optionSelected ? optionSelected[keyOfText] : this.config.empty;
    };
    /**
     * @param {?} currentValue
     * @param {?} keyOfValue
     * @param {?} options
     * @return {?}
     */
    InputSelectComponent.prototype.getOptionSelected = function (currentValue, keyOfValue, options) {
        var /** @type {?} */ optionSelected;
        for (var _i = 0, options_1 = options; _i < options_1.length; _i++) {
            var option = options_1[_i];
            if (this.isAnOptionWithChildren(option)) {
                optionSelected = this.getOptionSelected(currentValue, keyOfValue, /** @type {?} */ ((option.children)));
            }
            else {
                var /** @type {?} */ typeOfValue = typeof option[keyOfValue];
                /**
                 * If the type is a number, the equal must be soft to match, ex:
                 *      1 == "1" -> true
                 *
                 * If the type is other, the equiality can be hard, because,
                 * when the currentValue is a string that contains "[object Object]"
                 * if you test it against an object, it will be true, ex:
                 * "[object Object]" == {} -> true
                 * "[object Object]" === {} -> false
                 *
                 */
                if (typeOfValue === "string" || typeOfValue === "number") {
                    // tslint:disable-next-line:triple-equals
                    optionSelected = option[keyOfValue] == currentValue ? option : undefined;
                }
                else {
                    optionSelected = option[keyOfValue] === currentValue ? option : undefined;
                }
            }
            if (optionSelected) {
                break;
            }
        }
        return optionSelected;
    };
    /**
     * @param {?} value
     * @return {?}
     */
    InputSelectComponent.prototype.isEmpty = function (value) {
        var _a = this.config.options, keyOfValue = _a.value, options = _a.data;
        return this.getOptionSelected(value, keyOfValue, options) == null;
    };
    /**
     * @param {?} options
     * @return {?}
     */
    InputSelectComponent.prototype.isAnOptionWithChildren = function (options) {
        return options.children != null && options.children instanceof Array;
    };
    return InputSelectComponent;
}(InputBase));
InputSelectComponent.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                selector: "inline-editor-select",
                styles: ["a {     text-decoration: none;     color: #428bca;     border-bottom: dashed 1px #428bca;     cursor: pointer;     line-height: 2;     margin-right: 5px;     margin-left: 5px; }   /* editable-empty */  .editable-empty, .editable-empty:hover, .editable-empty:focus, a.editable-empty, a.editable-empty:hover, a.editable-empty:focus {     font-style: italic;     color: #DD1144;     text-decoration: none; }  .inlineEditForm {     display: inline-block;     white-space: nowrap;     margin: 0; }  #inlineEditWrapper {     display: inline-block; }  .inlineEditForm input, select {     width: auto;     display: inline; }  .editInvalid {     color: #a94442;     margin-bottom: 0; }  .error {     border-color: #a94442; }  [hidden] {     display: none; }"],
                template: "\n    <select #inputRef class=\"form-control\" [(ngModel)]=\"value\"\n    (focus)=\"onFocus($event)\" (keypress)=\"onKeyPress($event)\" (blur)=\"onBlur($event)\" (click)=\"onClick($event)\"\n    (keypress.enter)=\"onEnter($event)\" (keypress.escape)=\"onEscape($event)\" [disabled]=\"state.isDisabled()\">\n        <ng-template ngFor let-option [ngForOf]=\"config.options.data\">\n            <optgroup *ngIf=\"option.children\" [label]=\"option[config.options.text]\">\n                <option *ngFor=\"let child of option.children\" [ngValue]=\"child[config.options.value]\">\n                    {{child[config.options.text]}}\n                </option>\n            </optgroup>\n            <option *ngIf=\"!option.children\" [ngValue]=\"option[config.options.value]\">\n                {{option[config.options.text]}}\n            </option>\n        </ng-template>\n    </select>\n            ",
                changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush,
            },] },
];
/**
 * @nocollapse
 */
InputSelectComponent.ctorParameters = function () { return [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"], },
]; };
var InputDateComponent = (function (_super) {
    __extends(InputDateComponent, _super);
    /**
     * @param {?} injector
     */
    function InputDateComponent(injector) {
        var _this = _super.call(this, injector) || this;
        _this.isRegexTestable = true;
        return _this;
    }
    return InputDateComponent;
}(InputBase));
InputDateComponent.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                selector: "inline-editor-date",
                styles: ["a {     text-decoration: none;     color: #428bca;     border-bottom: dashed 1px #428bca;     cursor: pointer;     line-height: 2;     margin-right: 5px;     margin-left: 5px; }   /* editable-empty */  .editable-empty, .editable-empty:hover, .editable-empty:focus, a.editable-empty, a.editable-empty:hover, a.editable-empty:focus {     font-style: italic;     color: #DD1144;     text-decoration: none; }  .inlineEditForm {     display: inline-block;     white-space: nowrap;     margin: 0; }  #inlineEditWrapper {     display: inline-block; }  .inlineEditForm input, select {     width: auto;     display: inline; }  .editInvalid {     color: #a94442;     margin-bottom: 0; }  .error {     border-color: #a94442; }  [hidden] {     display: none; }"],
                template: "<input #inputRef type=\"date\" class=\"form-control\" (keyup.enter)=\"onEnter($event)\"\n                (keyup.escape)=\"onEscape($event)\" (focus)=\"onFocus($event)\" (blur)=\"onBlur($event)\" (blur)=\"onBlur($event)\"\n                (keypress)=\"onKeyPress($event)\" [(ngModel)]=\"value\" [required]=\"config.required\"\n                [disabled]=\"state.isDisabled()\" [name]=\"config.name\" [placeholder]=\"config.placeholder\"\n                [size]=\"config.size\" [min]=\"config.min\" [max]=\"config.max\"/>",
                changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush,
            },] },
];
/**
 * @nocollapse
 */
InputDateComponent.ctorParameters = function () { return [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"], },
]; };
var InputTimeComponent = (function (_super) {
    __extends(InputTimeComponent, _super);
    /**
     * @param {?} injector
     */
    function InputTimeComponent(injector) {
        var _this = _super.call(this, injector) || this;
        _this.isRegexTestable = true;
        return _this;
    }
    return InputTimeComponent;
}(InputBase));
InputTimeComponent.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                selector: "inline-editor-time",
                styles: ["a {     text-decoration: none;     color: #428bca;     border-bottom: dashed 1px #428bca;     cursor: pointer;     line-height: 2;     margin-right: 5px;     margin-left: 5px; }   /* editable-empty */  .editable-empty, .editable-empty:hover, .editable-empty:focus, a.editable-empty, a.editable-empty:hover, a.editable-empty:focus {     font-style: italic;     color: #DD1144;     text-decoration: none; }  .inlineEditForm {     display: inline-block;     white-space: nowrap;     margin: 0; }  #inlineEditWrapper {     display: inline-block; }  .inlineEditForm input, select {     width: auto;     display: inline; }  .editInvalid {     color: #a94442;     margin-bottom: 0; }  .error {     border-color: #a94442; }  [hidden] {     display: none; }"],
                template: "<input #inputRef type=\"time\" class=\"form-control\" (keyup.enter)=\"onEnter($event)\"\n                (keyup.escape)=\"onEscape($event)\" (focus)=\"onFocus($event)\" (blur)=\"onBlur($event)\" (click)=\"onClick($event)\"\n                (keypress)=\"onKeyPress($event)\" [(ngModel)]=\"value\" [required]=\"config.required\"\n                [disabled]=\"config.disabled\" [name]=\"config.name\" [placeholder]=\"config.placeholder\"\n                [size]=\"config.size\" [min]=\"config.min\" [max]=\"config.max\"/>",
                changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush,
            },] },
];
/**
 * @nocollapse
 */
InputTimeComponent.ctorParameters = function () { return [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"], },
]; };
var InputDatetimeComponent = (function (_super) {
    __extends(InputDatetimeComponent, _super);
    /**
     * @param {?} injector
     */
    function InputDatetimeComponent(injector) {
        var _this = _super.call(this, injector) || this;
        _this.isRegexTestable = true;
        return _this;
    }
    return InputDatetimeComponent;
}(InputBase));
InputDatetimeComponent.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                selector: "inline-editor-datetime",
                styles: ["a {     text-decoration: none;     color: #428bca;     border-bottom: dashed 1px #428bca;     cursor: pointer;     line-height: 2;     margin-right: 5px;     margin-left: 5px; }   /* editable-empty */  .editable-empty, .editable-empty:hover, .editable-empty:focus, a.editable-empty, a.editable-empty:hover, a.editable-empty:focus {     font-style: italic;     color: #DD1144;     text-decoration: none; }  .inlineEditForm {     display: inline-block;     white-space: nowrap;     margin: 0; }  #inlineEditWrapper {     display: inline-block; }  .inlineEditForm input, select {     width: auto;     display: inline; }  .editInvalid {     color: #a94442;     margin-bottom: 0; }  .error {     border-color: #a94442; }  [hidden] {     display: none; }"],
                template: "<input #inputRef type=\"datetime-local\" class=\"form-control\" (keyup.enter)=\"onEnter($event)\"\n                (keyup.escape)=\"onEscape($event)\" (focus)=\"onFocus($event)\" (blur)=\"onBlur($event)\" (blur)=\"onBlur($event)\"\n                (keypress)=\"onKeyPress($event)\" [(ngModel)]=\"value\" [required]=\"config.required\"\n                [disabled]=\"state.isDisabled()\" [name]=\"config.name\" [placeholder]=\"config.placeholder\"\n                [size]=\"config.size\" [min]=\"config.min\" [max]=\"config.max\"/>",
                changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush,
            },] },
];
/**
 * @nocollapse
 */
InputDatetimeComponent.ctorParameters = function () { return [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injector"], },
]; };
var InternalEvents = (function () {
    function InternalEvents() {
        this.onUpdateStateOfParent = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onUpdateStateOfChild = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onFocus = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onBlur = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onKeyPress = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onEnter = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onEscape = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onSave = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onEdit = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onCancel = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onClick = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onUpdateConfig = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    return InternalEvents;
}());
var ExternalEvents = (function () {
    function ExternalEvents() {
        this.onChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onSave = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onKeyPress = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onFocus = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onBlur = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onEnter = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onEscape = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onEdit = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onCancel = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onClick = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.onError = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    return ExternalEvents;
}());
var InlineEditorState = (function () {
    /**
     * @param {?=} __0
     */
    function InlineEditorState(_a) {
        var _b = _a === void 0 ? { value: "" } : _a, value = _b.value, _c = _b.disabled, disabled = _c === void 0 ? false : _c, _d = _b.editing, editing = _d === void 0 ? false : _d, _e = _b.empty, empty = _e === void 0 ? false : _e;
        this.value = value;
        this.disabled = disabled;
        this.editing = editing;
        this.empty = empty;
    }
    /**
     * @param {?} state
     * @return {?}
     */
    InlineEditorState.prototype.newState = function (state) {
        return new InlineEditorState(state instanceof InlineEditorState ?
            state.getState() : state);
    };
    /**
     * @return {?}
     */
    InlineEditorState.prototype.getState = function () {
        var _a = this, value = _a.value, editing = _a.editing, disabled = _a.disabled, empty = _a.empty;
        return {
            value: value,
            editing: editing,
            disabled: disabled,
            empty: empty,
        };
    };
    /**
     * @return {?}
     */
    InlineEditorState.prototype.clone = function () {
        return this.newState(this);
    };
    /**
     * @return {?}
     */
    InlineEditorState.prototype.isEmpty = function () {
        return this.empty;
    };
    /**
     * @return {?}
     */
    InlineEditorState.prototype.isEditing = function () {
        return this.editing;
    };
    /**
     * @return {?}
     */
    InlineEditorState.prototype.isDisabled = function () {
        return this.disabled;
    };
    return InlineEditorState;
}());
var defaultConfig = {
    name: "",
    required: false,
    options: {
        data: [],
        text: "text",
        value: "value",
    },
    empty: "empty",
    placeholder: "placeholder",
    type: "text",
    size: 8,
    min: 0,
    max: Infinity,
    cols: 10,
    rows: 4,
    pattern: "",
    disabled: false,
    saveOnBlur: false,
    saveOnChange: false,
    saveOnEnter: true,
    editOnClick: true,
    cancelOnEscape: true,
    hideButtons: false,
    onlyValue: true,
    checkedText: "Check",
    uncheckedText: "Uncheck",
};
var InlineEditorComponent = (function () {
    /**
     * @param {?} componentFactoryResolver
     */
    function InlineEditorComponent(componentFactoryResolver) {
        this.componentFactoryResolver = componentFactoryResolver;
        this.events = {
            internal: new InternalEvents(),
            external: new ExternalEvents(),
        };
        this.onChange = this.events.external.onChange;
        this.onSave = this.events.external.onSave;
        this.onEdit = this.events.external.onEdit;
        this.onCancel = this.events.external.onCancel;
        this.onError = this.events.external.onError;
        this.onEnter = this.events.external.onEnter;
        this.onEscape = this.events.external.onEscape;
        this.onKeyPress = this.events.external.onKeyPress;
        this.onFocus = this.events.external.onFocus;
        this.onBlur = this.events.external.onBlur;
        this.onClick = this.events.external.onClick;
        this.subscriptions = {};
        this.components = {
            text: InputTextComponent,
            number: InputNumberComponent,
            password: InputPasswordComponent,
            range: InputRangeComponent,
            textarea: InputTextareaComponent,
            select: InputSelectComponent,
            date: InputDateComponent,
            time: InputTimeComponent,
            datetime: InputDatetimeComponent,
            checkbox: InputCheckboxComponent,
        };
        this.isEnterKeyPressed = false;
    }
    Object.defineProperty(InlineEditorComponent.prototype, "empty", {
        /**
         * @return {?}
         */
        get: function () {
            return this._empty;
        },
        /**
         * @param {?} empty
         * @return {?}
         */
        set: function (empty) {
            this._empty = empty;
            this.updateConfig(undefined, "empty", empty);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InlineEditorComponent.prototype, "checkedText", {
        /**
         * @return {?}
         */
        get: function () {
            return this._checkedText;
        },
        /**
         * @param {?} checkedText
         * @return {?}
         */
        set: function (checkedText) {
            this._checkedText = checkedText;
            this.updateConfig(undefined, "checkedText", checkedText);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InlineEditorComponent.prototype, "uncheckedText", {
        /**
         * @return {?}
         */
        get: function () {
            return this._uncheckedText;
        },
        /**
         * @param {?} uncheckedText
         * @return {?}
         */
        set: function (uncheckedText) {
            this._uncheckedText = uncheckedText;
            this.updateConfig(undefined, "uncheckedText", uncheckedText);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InlineEditorComponent.prototype, "saveOnEnter", {
        /**
         * @return {?}
         */
        get: function () {
            return this._saveOnEnter;
        },
        /**
         * @param {?} saveOnEnter
         * @return {?}
         */
        set: function (saveOnEnter) {
            this._saveOnEnter = saveOnEnter;
            this.updateConfig(undefined, "saveOnEnter", saveOnEnter);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InlineEditorComponent.prototype, "saveOnBlur", {
        /**
         * @return {?}
         */
        get: function () {
            return this._saveOnBlur;
        },
        /**
         * @param {?} saveOnBlur
         * @return {?}
         */
        set: function (saveOnBlur) {
            this._saveOnBlur = saveOnBlur;
            this.updateConfig(undefined, "saveOnBlur", saveOnBlur);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InlineEditorComponent.prototype, "saveOnChange", {
        /**
         * @return {?}
         */
        get: function () {
            return this._saveOnChange;
        },
        /**
         * @param {?} saveOnChange
         * @return {?}
         */
        set: function (saveOnChange) {
            this._saveOnChange = saveOnChange;
            this.updateConfig(undefined, "saveOnChange", saveOnChange);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InlineEditorComponent.prototype, "editOnClick", {
        /**
         * @return {?}
         */
        get: function () {
            return this._editOnClick;
        },
        /**
         * @param {?} editOnClick
         * @return {?}
         */
        set: function (editOnClick) {
            this._editOnClick = editOnClick;
            this.updateConfig(undefined, "editOnClick", editOnClick);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InlineEditorComponent.prototype, "cancelOnEscape", {
        /**
         * @return {?}
         */
        get: function () {
            return this._cancelOnEscape;
        },
        /**
         * @param {?} cancelOnEscape
         * @return {?}
         */
        set: function (cancelOnEscape) {
            this._cancelOnEscape = cancelOnEscape;
            this.updateConfig(undefined, "cancelOnEscape", cancelOnEscape);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InlineEditorComponent.prototype, "hideButtons", {
        /**
         * @return {?}
         */
        get: function () {
            return this._hideButtons;
        },
        /**
         * @param {?} hideButtons
         * @return {?}
         */
        set: function (hideButtons) {
            this._hideButtons = hideButtons;
            this.updateConfig(undefined, "hideButtons", hideButtons);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InlineEditorComponent.prototype, "disabled", {
        /**
         * @return {?}
         */
        get: function () {
            return this._disabled;
        },
        /**
         * @param {?} disabled
         * @return {?}
         */
        set: function (disabled) {
            this._disabled = disabled;
            this.updateConfig(undefined, "disabled", disabled);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InlineEditorComponent.prototype, "required", {
        /**
         * @return {?}
         */
        get: function () {
            return this._required;
        },
        /**
         * @param {?} required
         * @return {?}
         */
        set: function (required) {
            this._required = required;
            this.updateConfig(undefined, "required", required);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InlineEditorComponent.prototype, "onlyValue", {
        /**
         * @return {?}
         */
        get: function () {
            return this._onlyValue;
        },
        /**
         * @param {?} onlyValue
         * @return {?}
         */
        set: function (onlyValue) {
            this._onlyValue = onlyValue;
            this.updateConfig(undefined, "onlyValue", onlyValue);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InlineEditorComponent.prototype, "placeholder", {
        /**
         * @return {?}
         */
        get: function () {
            return this._placeholder;
        },
        /**
         * @param {?} placeholder
         * @return {?}
         */
        set: function (placeholder) {
            this._placeholder = placeholder;
            this.updateConfig(undefined, "placeholder", placeholder);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InlineEditorComponent.prototype, "name", {
        /**
         * @return {?}
         */
        get: function () {
            return this._name;
        },
        /**
         * @param {?} name
         * @return {?}
         */
        set: function (name) {
            this._name = name;
            this.updateConfig(undefined, "name", name);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InlineEditorComponent.prototype, "pattern", {
        /**
         * @return {?}
         */
        get: function () {
            return this._pattern;
        },
        /**
         * @param {?} pattern
         * @return {?}
         */
        set: function (pattern) {
            this._pattern = pattern;
            this.updateConfig(undefined, "pattern", pattern);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InlineEditorComponent.prototype, "size", {
        /**
         * @return {?}
         */
        get: function () {
            return this._size;
        },
        /**
         * @param {?} size
         * @return {?}
         */
        set: function (size) {
            this._size = size;
            this.updateConfig(undefined, "size", size);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InlineEditorComponent.prototype, "min", {
        /**
         * @return {?}
         */
        get: function () {
            return this._min;
        },
        /**
         * @param {?} min
         * @return {?}
         */
        set: function (min) {
            this._min = min;
            this.updateConfig(undefined, "min", min);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InlineEditorComponent.prototype, "max", {
        /**
         * @return {?}
         */
        get: function () {
            return this._max;
        },
        /**
         * @param {?} max
         * @return {?}
         */
        set: function (max) {
            this._max = max;
            this.updateConfig(undefined, "max", max);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InlineEditorComponent.prototype, "cols", {
        /**
         * @return {?}
         */
        get: function () {
            return this._cols;
        },
        /**
         * @param {?} cols
         * @return {?}
         */
        set: function (cols) {
            this._cols = cols;
            this.updateConfig(undefined, "cols", cols);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InlineEditorComponent.prototype, "rows", {
        /**
         * @return {?}
         */
        get: function () {
            return this._rows;
        },
        /**
         * @param {?} rows
         * @return {?}
         */
        set: function (rows) {
            this._rows = rows;
            this.updateConfig(undefined, "rows", rows);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(InlineEditorComponent.prototype, "options", {
        /**
         * @return {?}
         */
        get: function () {
            return this._options;
        },
        /**
         * @param {?} options
         * @return {?}
         */
        set: function (options) {
            this._options = options;
            this.updateConfig(undefined, "options", options);
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    InlineEditorComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.config = this.generateSafeConfig();
        this.state = new InlineEditorState({
            disabled: this.config.disabled,
            value: "",
        });
        this.service = new InlineEditorService(this.events, Object.assign({}, this.config));
        this.subscriptions.onUpdateStateSubcription = this.events.internal.onUpdateStateOfParent.subscribe(function (state) { return _this.state = state; });
        this.subscriptions.onSaveSubscription = this.events.internal.onSave.subscribe(function (_a) {
            var event = _a.event, state = _a.state;
            return _this.save({
                event: event,
                state: state.getState(),
            });
        });
        this.subscriptions.onCancelSubscription = this.events.internal.onCancel.subscribe(function (_a) {
            var event = _a.event, state = _a.state;
            return _this.cancel({
                event: event,
                state: state.getState(),
            });
        });
        this.subscriptions.onChangeSubcription = this.events.internal.onChange.subscribe(function (_a) {
            var event = _a.event, state = _a.state;
            if (_this.config.saveOnChange) {
                _this.saveAndClose({
                    event: event,
                    state: state.getState(),
                });
            }
            _this.emit(_this.onChange, {
                event: event,
                state: state.getState(),
            });
        });
        this.subscriptions.onKeyPressSubcription = this.events.internal.onKeyPress.subscribe(function (_a) {
            var event = _a.event, state = _a.state;
            return _this.emit(_this.onKeyPress, {
                event: event,
                state: state.getState(),
            });
        });
        this.subscriptions.onBlurSubscription = this.events.internal.onBlur.subscribe(function (_a) {
            var event = _a.event, state = _a.state;
            // TODO (xxxtonixx): Maybe, this approach is not the best,
            // because we need to set a class property and it is dangerous.
            // We should search for one better.
            var /** @type {?} */ isSavedByEnterKey = _this.isEnterKeyPressed && _this.config.saveOnEnter;
            if (_this.config.saveOnBlur && !isSavedByEnterKey) {
                _this.saveAndClose({
                    event: event,
                    state: state.getState(),
                });
            }
            _this.isEnterKeyPressed = false;
            _this.emit(_this.onBlur, {
                event: event,
                state: state.getState(),
            });
        });
        this.subscriptions.onClickSubcription = this.events.internal.onClick.subscribe(function (_a) {
            var event = _a.event, state = _a.state;
            return _this.emit(_this.onClick, {
                event: event,
                state: state.getState(),
            });
        });
        this.subscriptions.onFocusSubcription = this.events.internal.onFocus.subscribe(function (_a) {
            var event = _a.event, state = _a.state;
            return _this.emit(_this.onFocus, {
                event: event,
                state: state.getState(),
            });
        });
        this.subscriptions.onEnterSubscription = this.events.internal.onEnter.subscribe(function (_a) {
            var event = _a.event, state = _a.state;
            _this.isEnterKeyPressed = true;
            if (_this.config.saveOnEnter) {
                _this.save({
                    event: event,
                    state: state.getState(),
                });
                _this.edit({ editing: false });
            }
            _this.emit(_this.onEnter, {
                event: event,
                state: state.getState(),
            });
        });
        this.subscriptions.onEscapeSubscription = this.events.internal.onEscape.subscribe(function (_a) {
            var event = _a.event, state = _a.state;
            if (_this.config.cancelOnEscape) {
                _this.cancel({
                    event: event,
                    state: state.getState(),
                });
            }
            _this.emit(_this.onEscape, {
                event: event,
                state: state.getState(),
            });
        });
    };
    /**
     * @return {?}
     */
    InlineEditorComponent.prototype.ngAfterContentInit = function () {
        this.service.onUpdateStateOfService.emit(this.state.clone());
        this.generateComponent(this.config.type);
    };
    /**
     * @return {?}
     */
    InlineEditorComponent.prototype.ngOnDestroy = function () {
        Object.values(this.subscriptions).forEach(function (subscription) { return subscription.unsubscribe(); });
        this.currentComponent.destroy();
        this.service.destroy();
    };
    /**
     * @return {?}
     */
    InlineEditorComponent.prototype.validate = function () {
        var /** @type {?} */ errors = this.inputInstance ? this.inputInstance.checkValue() : [];
        return errors.length === 0 ? null : {
            InlineEditorError: {
                valid: false,
            },
        };
    };
    /**
     * @param {?} value
     * @return {?}
     */
    InlineEditorComponent.prototype.writeValue = function (value) {
        this.state = this.state.newState(Object.assign({}, this.state.getState(), { value: value }));
        this.events.internal.onUpdateStateOfChild.emit(this.state.clone());
    };
    /**
     * @param {?} refreshNGModel
     * @return {?}
     */
    InlineEditorComponent.prototype.registerOnChange = function (refreshNGModel) {
        this.refreshNGModel = refreshNGModel;
    };
    /**
     * @return {?}
     */
    InlineEditorComponent.prototype.registerOnTouched = function () { };
    /**
     * @param {?=} __0
     * @return {?}
     */
    InlineEditorComponent.prototype.edit = function (_a) {
        var _b = _a === void 0 ? {} : _a, _c = _b.editing, editing = _c === void 0 ? true : _c, _d = _b.focus, focus = _d === void 0 ? true : _d, _e = _b.select, select = _e === void 0 ? false : _e, event = _b.event;
        this.state = this.state.newState(Object.assign({}, this.state.getState(), { editing: editing }));
        this.events.internal.onUpdateStateOfChild.emit(this.state.clone());
        if (editing) {
            this.emit(this.onEdit, {
                event: event,
                state: this.state.getState(),
            });
        }
        if (editing && focus) {
            this.inputInstance.focus();
        }
        if (editing && select) {
            this.inputInstance.select();
        }
    };
    /**
     * @param {?} __0
     * @return {?}
     */
    InlineEditorComponent.prototype.save = function (_a) {
        var event = _a.event, hotState = _a.state;
        var /** @type {?} */ prevState = this.state.getState();
        var /** @type {?} */ state = Object.assign({}, prevState, hotState);
        var /** @type {?} */ errors = this.inputInstance.checkValue();
        if (errors.length !== 0) {
            this.onError.emit(errors);
        }
        else {
            this.state = this.state.newState(state);
            this.refreshNGModel(state.value);
            this.emit(this.onSave, {
                event: event,
                state: state,
            });
        }
    };
    /**
     * @param {?} outsideEvent
     * @return {?}
     */
    InlineEditorComponent.prototype.saveAndClose = function (outsideEvent) {
        this.save(outsideEvent);
        this.edit({ editing: false });
    };
    /**
     * @param {?} outsideEvent
     * @return {?}
     */
    InlineEditorComponent.prototype.cancel = function (outsideEvent) {
        this.edit({ editing: false });
        this.emit(this.onCancel, outsideEvent);
    };
    /**
     * @return {?}
     */
    InlineEditorComponent.prototype.getHotState = function () {
        return this.inputInstance.state.getState();
    };
    /**
     * @return {?}
     */
    InlineEditorComponent.prototype.showText = function () {
        return this.inputInstance ? this.inputInstance.showText() : "Loading...";
    };
    /**
     * @param {?} typeName
     * @return {?}
     */
    InlineEditorComponent.prototype.getComponentType = function (typeName) {
        var /** @type {?} */ type = this.components[typeName];
        if (!type) {
            throw new Error("That type does not exist or it is not implemented yet!");
        }
        return type;
    };
    /**
     * @param {?} type
     * @return {?}
     */
    InlineEditorComponent.prototype.generateComponent = function (type) {
        var /** @type {?} */ componentType = this.getComponentType(type);
        this.inputInstance = this.createInputInstance(componentType);
    };
    /**
     * @param {?} componentType
     * @return {?}
     */
    InlineEditorComponent.prototype.createInputInstance = function (componentType) {
        var /** @type {?} */ providers = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ReflectiveInjector"].resolve([{
                provide: InlineEditorService,
                useValue: this.service,
            }]);
        var /** @type {?} */ injector = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ReflectiveInjector"].fromResolvedProviders(providers, this.container.parentInjector);
        var /** @type {?} */ factory = this.componentFactoryResolver.resolveComponentFactory(componentType);
        this.componentRef = factory.create(injector);
        this.container.insert(this.componentRef.hostView);
        if (this.currentComponent) {
            this.currentComponent.destroy();
        }
        this.currentComponent = this.componentRef;
        return (this.componentRef.instance);
    };
    /**
     * @template T
     * @param {?} object
     * @return {?}
     */
    InlineEditorComponent.prototype.removeUndefinedProperties = function (object) {
        return JSON.parse(JSON.stringify(typeof object === "object" ? object : {}));
    };
    /**
     * @return {?}
     */
    InlineEditorComponent.prototype.generateSafeConfig = function () {
        var /** @type {?} */ configFromAttrs = {
            type: /** @type {?} */ ((this.type)),
            name: /** @type {?} */ ((this.name)),
            size: /** @type {?} */ ((this.size)),
            placeholder: /** @type {?} */ ((this.placeholder)),
            empty: /** @type {?} */ ((this.empty)),
            required: /** @type {?} */ ((this.required)),
            disabled: /** @type {?} */ ((this.disabled)),
            hideButtons: /** @type {?} */ ((this.hideButtons)),
            min: /** @type {?} */ ((this.min)),
            max: /** @type {?} */ ((this.max)),
            cols: /** @type {?} */ ((this.cols)),
            rows: /** @type {?} */ ((this.rows)),
            options: /** @type {?} */ ((this.options)),
            pattern: /** @type {?} */ ((this.pattern)),
            saveOnEnter: /** @type {?} */ ((this.saveOnEnter)),
            saveOnBlur: /** @type {?} */ ((this.saveOnBlur)),
            saveOnChange: /** @type {?} */ ((this.saveOnChange)),
            editOnClick: /** @type {?} */ ((this.editOnClick)),
            cancelOnEscape: /** @type {?} */ ((this.cancelOnEscape)),
            onlyValue: /** @type {?} */ ((this.onlyValue)),
            checkedText: /** @type {?} */ ((this.checkedText)),
            uncheckedText: /** @type {?} */ ((this.uncheckedText)),
        };
        return Object.assign({}, defaultConfig, this.removeUndefinedProperties(this.config), this.removeUndefinedProperties(configFromAttrs));
    };
    /**
     * @param {?=} config
     * @param {?=} property
     * @param {?=} value
     * @return {?}
     */
    InlineEditorComponent.prototype.updateConfig = function (config, property, value) {
        if (this.config) {
            config = config || this.config;
            if (property) {
                config[property] = value;
            }
            this.config = Object.assign({}, config);
            this.events.internal.onUpdateConfig.emit(this.config);
        }
    };
    /**
     * @param {?} event
     * @param {?} data
     * @return {?}
     */
    InlineEditorComponent.prototype.emit = function (event, data) {
        if (this.config.onlyValue) {
            event.emit(data.state.value);
        }
        else {
            ((event))
                .emit(Object.assign({}, data, { instance: this }));
        }
    };
    return InlineEditorComponent;
}());
InlineEditorComponent.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                selector: "inline-editor",
                template: "<div id=\"inlineEditWrapper\">   <a [ngClass]=\"{'editable-empty': state.isEmpty(), 'c-inline-editor': true }\" (click)=\"!config.editOnClick || edit()\" [hidden]=\"state.isEditing() && !config.disabled\">{{ showText() }}</a>   <div class=\"c-inline-editor inlineEditForm form-inline\" [hidden]=\"!state.isEditing() || config.disabled\">     <div class=\"form-group\">       <div #container></div>       <span *ngIf=\"!config.hideButtons\" class=\"c-inline-editor inline-editor-button-group\">       <button id=\"inline-editor-button-save\" class=\"btn btn-xs btn-primary c-inline-editor\"           (click)=\"saveAndClose({ event: $event, state: service.getState() })\">           <span class=\"fa fa-check\"></span>       </button>       <button class=\"btn btn-xs btn-danger c-inline-editor\" (click)=\"cancel({ event: $event, state: service.getState() })\"><span class=\"fa fa-remove\"></span> </button>       </span>      </div>   </div> </div>",
                providers: [
                    {
                        provide: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NG_VALUE_ACCESSOR"],
                        useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["forwardRef"])(function () { return InlineEditorComponent; }),
                        multi: true,
                    },
                    {
                        provide: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NG_VALIDATORS"],
                        useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["forwardRef"])(function () { return InlineEditorComponent; }),
                        multi: true,
                    },
                ],
                entryComponents: [
                    InputTextComponent,
                    InputNumberComponent,
                    InputPasswordComponent,
                    InputRangeComponent,
                    InputTextareaComponent,
                    InputSelectComponent,
                    InputDateComponent,
                    InputTimeComponent,
                    InputDatetimeComponent,
                    InputCheckboxComponent,
                ],
                changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush,
            },] },
];
/**
 * @nocollapse
 */
InlineEditorComponent.ctorParameters = function () { return [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ComponentFactoryResolver"], },
]; };
InlineEditorComponent.propDecorators = {
    'type': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'config': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'onChange': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] },],
    'onSave': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] },],
    'onEdit': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] },],
    'onCancel': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] },],
    'onError': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] },],
    'onEnter': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] },],
    'onEscape': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] },],
    'onKeyPress': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] },],
    'onFocus': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] },],
    'onBlur': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] },],
    'onClick': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] },],
    'empty': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'checkedText': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'uncheckedText': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'saveOnEnter': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'saveOnBlur': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'saveOnChange': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'editOnClick': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'cancelOnEscape': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'hideButtons': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'disabled': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'required': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'onlyValue': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'placeholder': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'name': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'pattern': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'size': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'min': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'max': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'cols': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'rows': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'options': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'container': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"], args: ["container", { read: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"] },] },],
};
var EXPORTS = [
    InputBase,
    InputTextComponent,
    InputNumberComponent,
    InputPasswordComponent,
    InputRangeComponent,
    InputTextareaComponent,
    InputSelectComponent,
    InputDateComponent,
    InputTimeComponent,
    InputDatetimeComponent,
    InputCheckboxComponent,
    InlineEditorComponent,
];
var InlineEditorModule = (function () {
    function InlineEditorModule() {
    }
    return InlineEditorModule;
}());
InlineEditorModule.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"], args: [{
                imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"]],
                declarations: EXPORTS,
                exports: [InlineEditorComponent],
            },] },
];
/**
 * @nocollapse
 */
InlineEditorModule.ctorParameters = function () { return []; };



/***/ }),

/***/ "./node_modules/ngx-gauge/ngx-gauge.es5.js":
/*!*************************************************!*\
  !*** ./node_modules/ngx-gauge/ngx-gauge.es5.js ***!
  \*************************************************/
/*! exports provided: NgxGaugeModule, ɵa, ɵb, ɵe, ɵc, ɵd */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NgxGaugeModule", function() { return NgxGaugeModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵa", function() { return NgxGauge; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵb", function() { return NgxGaugeAppend; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵe", function() { return NgxGaugeLabel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵc", function() { return NgxGaugePrepend; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵd", function() { return NgxGaugeValue; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");


/**
 * @param {?} value
 * @param {?} min
 * @param {?} max
 * @return {?}
 */
function clamp(value, min, max) {
    return Math.max(min, Math.min(max, value));
}
/**
 * @param {?} value
 * @return {?}
 */
/**
 * @param {?} value
 * @param {?=} fallbackValue
 * @return {?}
 */
function coerceNumberProperty(value, fallbackValue) {
    if (fallbackValue === void 0) { fallbackValue = 0; }
    return isNaN(parseFloat(value)) || isNaN(Number(value)) ? fallbackValue : Number(value);
}
/**
 * @param {?} value
 * @return {?}
 */
function cssUnit(value) {
    return value + "px";
}
/**
 * @param {?} value
 * @return {?}
 */
function isNumber(value) {
    return value != undefined && !isNaN(parseFloat(value)) && !isNaN(Number(value));
}
var NgxGaugeAppend = /** @class */ (function () {
    function NgxGaugeAppend() {
    }
    return NgxGaugeAppend;
}());
NgxGaugeAppend.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                selector: "ngx-gauge-append",
                exportAs: "ngxGaugeAppend"
            },] },
];
/**
 * @nocollapse
 */
NgxGaugeAppend.ctorParameters = function () { return []; };
var NgxGaugePrepend = /** @class */ (function () {
    function NgxGaugePrepend() {
    }
    return NgxGaugePrepend;
}());
NgxGaugePrepend.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                selector: "ngx-gauge-prepend",
                exportAs: "ngxGaugePrepend"
            },] },
];
/**
 * @nocollapse
 */
NgxGaugePrepend.ctorParameters = function () { return []; };
var NgxGaugeValue = /** @class */ (function () {
    function NgxGaugeValue() {
    }
    return NgxGaugeValue;
}());
NgxGaugeValue.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                selector: "ngx-gauge-value",
                exportAs: "ngxGaugeValue"
            },] },
];
/**
 * @nocollapse
 */
NgxGaugeValue.ctorParameters = function () { return []; };
var NgxGaugeLabel = /** @class */ (function () {
    function NgxGaugeLabel() {
    }
    return NgxGaugeLabel;
}());
NgxGaugeLabel.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                selector: "ngx-gauge-label",
                exportAs: "ngxGaugeLabel"
            },] },
];
/**
 * @nocollapse
 */
NgxGaugeLabel.ctorParameters = function () { return []; };
var DEFAULTS = {
    MIN: 0,
    MAX: 100,
    TYPE: 'arch',
    THICK: 4,
    FOREGROUND_COLOR: 'rgba(0, 150, 136, 1)',
    BACKGROUND_COLOR: 'rgba(0, 0, 0, 0.1)',
    CAP: 'butt',
    SIZE: 200
};
var NgxGauge = /** @class */ (function () {
    /**
     * @param {?} _elementRef
     * @param {?} _renderer
     */
    function NgxGauge(_elementRef, _renderer) {
        this._elementRef = _elementRef;
        this._renderer = _renderer;
        this._size = DEFAULTS.SIZE;
        this._min = DEFAULTS.MIN;
        this._max = DEFAULTS.MAX;
        this._initialized = false;
        this._animationRequestID = 0;
        this.max = DEFAULTS.MAX;
        this.type = (DEFAULTS.TYPE);
        this.cap = (DEFAULTS.CAP);
        this.thick = DEFAULTS.THICK;
        this.foregroundColor = DEFAULTS.FOREGROUND_COLOR;
        this.backgroundColor = DEFAULTS.BACKGROUND_COLOR;
        this.thresholds = Object.create(null);
        this._value = 0;
        this.duration = 1200;
    }
    Object.defineProperty(NgxGauge.prototype, "size", {
        /**
         * @return {?}
         */
        get: function () { return this._size; },
        /**
         * @param {?} value
         * @return {?}
         */
        set: function (value) {
            this._size = coerceNumberProperty(value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NgxGauge.prototype, "min", {
        /**
         * @return {?}
         */
        get: function () { return this._min; },
        /**
         * @param {?} value
         * @return {?}
         */
        set: function (value) {
            this._min = coerceNumberProperty(value, DEFAULTS.MIN);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NgxGauge.prototype, "value", {
        /**
         * @return {?}
         */
        get: function () { return this._value; },
        /**
         * @param {?} val
         * @return {?}
         */
        set: function (val) {
            this._value = coerceNumberProperty(val);
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} changes
     * @return {?}
     */
    NgxGauge.prototype.ngOnChanges = function (changes) {
        var /** @type {?} */ isTextChanged = changes['label'] || changes['append'] || changes['prepend'];
        var /** @type {?} */ isDataChanged = changes['value'] || changes['min'] || changes['max'];
        if (this._initialized) {
            if (isDataChanged) {
                var /** @type {?} */ nv = void 0, /** @type {?} */ ov = void 0;
                if (changes['value']) {
                    nv = changes['value'].currentValue;
                    ov = changes['value'].previousValue;
                }
                this._update(nv, ov);
            }
            else if (!isTextChanged) {
                this._destroy();
                this._init();
            }
        }
    };
    /**
     * @return {?}
     */
    NgxGauge.prototype._updateSize = function () {
        this._renderer.setElementStyle(this._elementRef.nativeElement, 'width', cssUnit(this._size));
        this._renderer.setElementStyle(this._elementRef.nativeElement, 'height', cssUnit(this._size));
    };
    /**
     * @return {?}
     */
    NgxGauge.prototype.ngAfterViewInit = function () {
        if (this._canvas) {
            this._init();
        }
    };
    /**
     * @return {?}
     */
    NgxGauge.prototype.ngOnDestroy = function () {
        this._destroy();
    };
    /**
     * @param {?} type
     * @return {?}
     */
    NgxGauge.prototype._getBounds = function (type) {
        var /** @type {?} */ head, /** @type {?} */ tail;
        if (type == 'semi') {
            head = Math.PI;
            tail = 2 * Math.PI;
        }
        else if (type == 'full') {
            head = 1.5 * Math.PI;
            tail = 3.5 * Math.PI;
        }
        else if (type === 'arch') {
            head = 0.8 * Math.PI;
            tail = 2.2 * Math.PI;
        }
        return { head: head, tail: tail };
    };
    /**
     * @param {?} start
     * @param {?} middle
     * @param {?} tail
     * @param {?} color
     * @return {?}
     */
    NgxGauge.prototype._drawShell = function (start, middle, tail, color) {
        var /** @type {?} */ center = this._getCenter(), /** @type {?} */ radius = this._getRadius();
        middle = Math.max(middle, start); // never below 0%
        middle = Math.min(middle, tail); // never exceed 100%
        this._clear();
        this._context.beginPath();
        this._context.strokeStyle = this.backgroundColor;
        this._context.arc(center.x, center.y, radius, middle, tail, false);
        this._context.stroke();
        this._context.beginPath();
        this._context.strokeStyle = color;
        this._context.arc(center.x, center.y, radius, start, middle, false);
        this._context.stroke();
    };
    /**
     * @return {?}
     */
    NgxGauge.prototype._clear = function () {
        this._context.clearRect(0, 0, this._getWidth(), this._getHeight());
    };
    /**
     * @return {?}
     */
    NgxGauge.prototype._getWidth = function () {
        return this.size;
    };
    /**
     * @return {?}
     */
    NgxGauge.prototype._getHeight = function () {
        return this.size;
    };
    /**
     * @return {?}
     */
    NgxGauge.prototype._getRadius = function () {
        var /** @type {?} */ center = this._getCenter();
        return center.x - this.thick;
    };
    /**
     * @return {?}
     */
    NgxGauge.prototype._getCenter = function () {
        var /** @type {?} */ x = this._getWidth() / 2, /** @type {?} */ y = this._getHeight() / 2;
        return { x: x, y: y };
    };
    /**
     * @return {?}
     */
    NgxGauge.prototype._init = function () {
        this._context = ((this._canvas.nativeElement)).getContext('2d');
        this._initialized = true;
        this._updateSize();
        this._setupStyles();
        this._create();
    };
    /**
     * @return {?}
     */
    NgxGauge.prototype._destroy = function () {
        if (this._animationRequestID) {
            window.cancelAnimationFrame(this._animationRequestID);
            this._animationRequestID = 0;
        }
        this._clear();
        this._context = null;
    };
    /**
     * @return {?}
     */
    NgxGauge.prototype._setupStyles = function () {
        this._context.canvas.width = this.size;
        this._context.canvas.height = this.size;
        this._context.lineCap = this.cap;
        this._context.lineWidth = this.thick;
    };
    /**
     * @param {?} value
     * @return {?}
     */
    NgxGauge.prototype._getForegroundColorByRange = function (value) {
        var /** @type {?} */ match = Object.keys(this.thresholds)
            .filter(function (item) { return isNumber(item) && Number(item) <= value; })
            .sort(function (a, b) { return Number(a) - Number(b); })
            .reverse()[0];
        return match !== undefined
            ? this.thresholds[match].color || this.foregroundColor
            : this.foregroundColor;
    };
    /**
     * @param {?=} nv
     * @param {?=} ov
     * @return {?}
     */
    NgxGauge.prototype._create = function (nv, ov) {
        var /** @type {?} */ self = this, /** @type {?} */ type = this.type, /** @type {?} */ bounds = this._getBounds(type), /** @type {?} */ duration = this.duration, /** @type {?} */ min = this.min, /** @type {?} */ max = this.max, /** @type {?} */ value = clamp(this.value, this.min, this.max), /** @type {?} */ start = bounds.head, /** @type {?} */ unit = (bounds.tail - bounds.head) / (max - min), /** @type {?} */ displacement = unit * (value - min), /** @type {?} */ tail = bounds.tail, /** @type {?} */ color = this._getForegroundColorByRange(value), /** @type {?} */ startTime;
        if (nv != undefined && ov != undefined) {
            displacement = unit * nv - unit * ov;
        }
        /**
         * @param {?} timestamp
         * @return {?}
         */
        function animate(timestamp) {
            timestamp = timestamp || new Date().getTime();
            var /** @type {?} */ runtime = timestamp - startTime;
            var /** @type {?} */ progress = Math.min(runtime / duration, 1);
            var /** @type {?} */ previousProgress = ov ? ov * unit : 0;
            var /** @type {?} */ middle = start + previousProgress + displacement * progress;
            self._drawShell(start, middle, tail, color);
            if (self._animationRequestID && (runtime < duration)) {
                self._animationRequestID = window.requestAnimationFrame(function (timestamp) { return animate(timestamp); });
            }
            else {
                window.cancelAnimationFrame(self._animationRequestID);
            }
        }
        self._animationRequestID = window.requestAnimationFrame(function (timestamp) {
            startTime = timestamp || new Date().getTime();
            animate(timestamp);
        });
    };
    /**
     * @param {?} nv
     * @param {?} ov
     * @return {?}
     */
    NgxGauge.prototype._update = function (nv, ov) {
        this._clear();
        this._create(nv, ov);
    };
    return NgxGauge;
}());
NgxGauge.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                selector: 'ngx-gauge',
                template: "\n      <div class=\"reading-block\" #reading [style.fontSize]=\"size * 0.22 + 'px'\" [style.lineHeight]=\"size + 'px'\">\n        <!-- This block can not be indented correctly, because line breaks cause layout spacing, related problem: https://pt.stackoverflow.com/q/276760/2998 -->\n        <u class=\"reading-affix\" [ngSwitch]=\"_prependChild != null\"><ng-content select=\"ngx-gauge-prepend\" *ngSwitchCase=\"true\"></ng-content><ng-container *ngSwitchCase=\"false\">{{prepend}}</ng-container></u><ng-container [ngSwitch]=\"_valueDisplayChild != null\"><ng-content *ngSwitchCase=\"true\" select=\"ngx-gauge-value\"></ng-content><ng-container *ngSwitchCase=\"false\">{{value | number}}</ng-container></ng-container><u class=\"reading-affix\" [ngSwitch]=\"_appendChild != null\"><ng-content select=\"ngx-gauge-append\" *ngSwitchCase=\"true\"></ng-content><ng-container *ngSwitchCase=\"false\">{{append}}</ng-container></u>\n      </div>\n      <div class=\"reading-label\" \n           [style.fontSize]=\"size / 13 + 'px'\" \n           [style.lineHeight]=\"(5 * size / 13) + size + 'px'\" \n           [ngSwitch]=\"_labelChild != null\">\n        <ng-content select=\"ngx-gauge-label\" *ngSwitchCase=\"true\"></ng-content>\n        <ng-container *ngSwitchCase=\"false\">{{label}}</ng-container>\n      </div>\n      <canvas #canvas [width]=\"size\" [height]=\"size\"></canvas>\n    ",
                styles: ["\n      .ngx-gauge-meter {\n          display: inline-block;\n          text-align: center;\n          position: relative;\n      }\n\n      .reading-block {\n          position: absolute;\n          width: 100%;\n          font-weight: normal;\n          white-space: nowrap;\n          text-align: center;\n          overflow: hidden;\n          text-overflow: ellipsis;\n      }\n\n      .reading-label {\n          font-family: inherit;\n          width: 100%;\n          display: inline-block;\n          position: absolute;\n          text-align: center;\n          white-space: nowrap;\n          overflow: hidden;\n          text-overflow: ellipsis;\n          font-weight: normal;\n      }\n\n      .reading-affix {\n          text-decoration: none;\n          font-size: 0.6em;\n          opacity: 0.8;\n          font-weight: 200;\n          padding: 0 0.18em;\n      }\n\n      .reading-affix:first-child {\n          padding-left: 0;\n      }\n\n      .reading-affix:last-child {\n          padding-right: 0;\n      }\n    "],
                host: {
                    'role': 'meter',
                    '[class.ngx-gauge-meter]': 'true',
                    '[attr.aria-valuemin]': 'min',
                    '[attr.aria-valuemax]': 'max',
                    '[attr.aria-valuenow]': 'value'
                },
                encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None
            },] },
];
/**
 * @nocollapse
 */
NgxGauge.ctorParameters = function () { return [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer"], },
]; };
NgxGauge.propDecorators = {
    '_canvas': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"], args: ['canvas',] },],
    '_labelChild': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"], args: [NgxGaugeLabel,] },],
    '_prependChild': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"], args: [NgxGaugePrepend,] },],
    '_appendChild': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"], args: [NgxGaugeAppend,] },],
    '_valueDisplayChild': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"], args: [NgxGaugeValue,] },],
    'size': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'min': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'max': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'type': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'cap': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'thick': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'label': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'append': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'prepend': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'foregroundColor': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'backgroundColor': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'thresholds': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'value': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'duration': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
};
var NgxGaugeModule = /** @class */ (function () {
    function NgxGaugeModule() {
    }
    return NgxGaugeModule;
}());
NgxGaugeModule.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"], args: [{
                imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"]],
                declarations: [NgxGauge, NgxGaugeAppend, NgxGaugePrepend, NgxGaugeValue, NgxGaugeLabel],
                exports: [NgxGauge, NgxGaugeAppend, NgxGaugePrepend, NgxGaugeValue, NgxGaugeLabel]
            },] },
];
/**
 * @nocollapse
 */
NgxGaugeModule.ctorParameters = function () { return []; };
/**
 * Generated bundle index. Do not edit.
 */

//# sourceMappingURL=ngx-gauge.es5.js.map


/***/ }),

/***/ "./src/app/pages/group-a/analytic-a1/analytic-a1.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/pages/group-a/analytic-a1/analytic-a1.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- \n<div class=\"row\"  *ngIf=\"records\">\n  <div class=\"col-xxxl-12 col-md-8\">\n    <nb-card size=\"xlarge\">\n      <h1 style=\"text-align: center;color:red\">Please Connect To Server...</h1>\n      <h2 style=\"text-align: center;color:lightgreen\">Then Try Relaoding The Page...</h2>\n      <img style=\"height:600px;width:80%;margin:0 auto;\" src=\"assets/images/error.png\">\n    </nb-card>\n  </div>\n</div> -->\n\n<div class=\"row\">\n  <div class=\"col-xxxl-8 col-md-8\">\n    <nb-card size=\"xxsmall\">\n      <h1 style=\"text-align: center;padding-top:2.5%\">Heat Map Analytics</h1>\n    </nb-card>\n  </div>\n  <div class=\"col-xxxl-2 col-md-2\">\n    <nb-card size=\"xxsmall\"  routerLink=\"/pages/addInfo\" style=\"cursor: pointer;\">\n      <h3 style=\"text-align: center;padding-top:12%;\">Add Cameras</h3>\n    </nb-card>\n  </div>\n  <div class=\"col-xxxl-2 col-md-2\">\n    <nb-card size=\"xxsmall\" routerLink=\"/pages/log\" style=\"cursor: pointer;\" >\n      <h3 style=\"text-align: center;padding-top:12%;\" >Log</h3>\n    </nb-card>\n  </div>\n</div>\n\n<div class=\"row\">\n  \n  <div class=\"col-xxxl-3 col-md-3\" *ngFor=\"let detail of details$\" style=\"cursor: pointer;\">\n    <ngx-status-card title={{detail.camera_name}} (click)=\"getInfo(detail.camera_id)\" (click)=\"getRectCoords(detail.camera_id)\" (click)=\"start(true)\">\n      <i class=\"ion ion-eye\"></i>\n    </ngx-status-card>\n    \n  </div>\n\n</div>\n\n<div class=\"row\">\n        <div class=\"col-xxxl-2 col-xxl-2 col-md-2\">\n  <nb-card>\n    <nb-tabset fullWidth>\n      <nb-tab tabTitle=\"Camera ID\">\n          <h1 style=\"text-align:center;padding-top:5%;font-size:50px;color:lightgreen\">{{infos$[0].camera_id}}</h1>\n      </nb-tab>\n      </nb-tabset>\n      </nb-card>\n    </div>\n  <div class=\"col-xxxl-2 col-xxl-2 col-md-2\">\n  <nb-card>\n    <nb-tabset fullWidth>\n      <nb-tab tabTitle=\"Current Session\">\n          <h1 style=\"text-align:center;padding-top:5%;font-size:50px;color:lightgreen\">{{infos$[0].currentSession}}</h1>\n      </nb-tab>\n      </nb-tabset>\n      </nb-card>\n      </div>\n    <div class=\"col-xxxl-2 col-xxl-2 col-md-2\">\n  <nb-card>\n    <nb-tabset fullWidth>\n      <nb-tab tabTitle=\"Crowd Count\" >\n        <h1 style=\"text-align:center;padding-top:5%;font-size:50px;color:lightgreen\">{{infos$[0].crowdCount}}</h1>\n      </nb-tab>\n      </nb-tabset>\n      </nb-card>\n      </div>\n    <div class=\"col-xxxl-2 col-xxl-2 col-md-2\">\n  <nb-card>\n    <nb-tabset fullWidth>\n      <nb-tab tabTitle=\"Dwell Time\">\n          <h1 style=\"text-align:center;padding-top:5%;font-size:50px;color:lightgreen\">{{infos$[0].dwellTime}}</h1>\n      </nb-tab>\n      </nb-tabset>\n      </nb-card>\n      </div>\n          <div class=\"col-xxxl-2 col-xxl-2 col-md-2\">\n  <nb-card>\n    <nb-tabset fullWidth>\n      <nb-tab tabTitle=\"Floorspace Utilizaton\">\n          <h1 style=\"text-align:center;padding-top:5%;font-size:50px;color:lightgreen\">{{infos$[0].totalUtilization}}</h1>\n      </nb-tab>\n      </nb-tabset>\n      </nb-card>\n      </div>\n      <div class=\"col-xxxl-2 col-xxl-2 col-md-2\">\n  <nb-card>\n    <nb-tabset fullWidth>\n      <nb-tab tabTitle=\"Peak Utilizaton\">\n          <h1 style=\"text-align:center;padding-top:5%;font-size:50px;color:lightgreen\">{{infos$[0].peakUtilization}}</h1>\n      </nb-tab>\n      </nb-tabset>\n      </nb-card>\n      </div> \n</div>\n\n<div class=\"row\" >\n   \n      <div class=\"col-xxxl-6 col-xxl-6 col-md-6\">\n      <nb-card >\n          <nb-card-header>\n          <div class=\"row\">\n              <div class=\"col-xxxl-4 col-xxl-6 col-md-6\"><nb-card></nb-card></div>\n              <div class=\"col-xxxl-4 col-xxl-6 col-md-6\"><h1 style=\"text-align:center\">Heat Map</h1></div>\n              <div class=\"col-xxxl-2 col-xxl-6 col-md-6\"><nb-card></nb-card></div>\n              <div class=\"col-xxxl-2 col-xxl-6 col-md-6\"><nb-card><h2 style=\"text-align:center;\" (click)=\"toggleEditor()\"> <nb-action icon=\"nb-edit\"></nb-action></h2></nb-card></div>\n\n          </div>\n        \n        <div class=\"row\" id=\"editor\" style=\"display:none;padding-top:10px\">\n          <div class=\"col-xxxl-3 col-xxl-6 col-md-6\">\n            <nb-card>\n              <h2 style=\"text-align:center;color: lightgreen;cursor:pointer\" (click)=\"drawOnCanvas()\">Draw</h2>\n            </nb-card>\n          </div>\n          <div class=\"col-xxxl-3 col-xxl-6 col-md-6\">\n            <nb-card>\n                <h2 style=\"text-align: center;color: lightgreen;cursor:pointer\" (click)=\"deleteOnCanvas()\">Delete</h2>\n            </nb-card>\n          </div>\n          <div class=\"col-xxxl-3 col-xxl-6 col-md-6\">\n              <nb-card>\n                <h2 style=\"text-align: center;color: lightgreen;cursor:pointer\" (click)=\"undo()\">Undo</h2>\n              </nb-card>\n            </div>\n           <div class=\"col-xxxl-3 col-xxl-6 col-md-6\">\n            <nb-card>\n                <h2 style=\"text-align: center;color:lightgreen;cursor:pointer\" (click)=\"closeEditor()\"> <nb-action icon=\"nb-close\"></nb-action></h2>\n            </nb-card>\n          </div>\n        </div>\n\n\n\n        <div class=\"row\" id=\"status\" style=\"display: none\">\n          <div class=\"col-xxxl-12 col-xxl-6 col-md-6\">\n            <h3 style=\"text-align:center;color:red\">Chosen Operation : {{status}}</h3>\n          </div>   \n        </div>\n        <!-- <h2 style=\"float:right\" (click)=\"undo()\">UNDO</h2> -->\n        \n          </nb-card-header>\n          <nb-card-body class=\"p-0\">\n          <!-- <div  id=\"canvas\" [ngStyle]=\"{'background-image': 'url(' + heatmap + ')'}\"> -->\n            <!-- <img src={{infos$[0].heatmap}} style=\"width:100%\"> -->\n            <!-- <div *ngFor=\"let coord of coords$\">\n                <div id=\"rectangle\" [ngStyle]=\"{'width': coord.width,'height': coord.height,'top':coord.top_length,'left':coord.left_length}\">\n                    <h1 style=\"text-align: center;\">{{coord.text}}</h1>\n                </div> -->\n                <!-- <h1>{{coord.width}}</h1> -->\n            <!-- </div> -->\n            <!-- <div id=\"rectangle\" style=\"left: 253px; top: 86.8594px; width: 97px; height: 76px;\"></div> -->\n            <!-- </div> -->\n            \n<img #img src=\"{{heatmap}}\" (load)=\"start(true)\" style='display: none;' />\n<canvas #canvasEl width={{imgWidth}} height={{imgHeight}} style=\"background:lightgray;\"  style=\"cursor:auto\" (mousedown)=\"point_it($event)\"   (mousedown)=\"IsInPath($event)\"  (mousedown)=\"delete()\" ></canvas>\n          </nb-card-body>\n        </nb-card>\n      \n  \n      </div>\n\n<div class=\"col-xxxl-6 col-xxl-6 col-md-6\">\n<div class=\"row\">\n      <div class=\"col-xxxl-12 col-xxl-12 col-md-12\">\n          <nb-card size=\"\">\n              <nb-tabset fullWidth>\n                <nb-tab tabTitle=\"Timestamp\">\n                    <h2 style=\"text-align:center;padding-top:2%;font-size:20px;color:lightgreen\">{{infos$[0].timestamp | date: 'dd:MM:yyyy'}}</h2>\n                    <h1 style=\"text-align:center;font-size:50px;color:red\">{{infos$[0].timestamp | date: 'h:mm:ss'}}</h1>\n                </nb-tab>\n                </nb-tabset>\n                </nb-card>\n          </div>\n  </div>\n<div class=\"row\">\n          <div class=\"col-xxxl-12 col-xxl-12 col-md-12\">\n\n              <nb-card size=\"medium\" style=\"overflow-y: auto\">\n                  <nb-tabset fullWidth>\n                    <nb-tab tabTitle=\"Log Table\" >\n                        <!-- <table class=\"table table-hover\">\n                            <thead  style=\"border:2px solid blue\">\n                                <tr>\n                                    <td><h2>CameraId</h2></td>\n                                    <td><h2>CrowdCount</h2></td>\n                                    <td><h2>DwellTime</h2></td>\n                                    <td><h2>CurrentSession</h2></td>\n                                </tr>\n                              </thead>\n                            <tbody>\n                                <tr *ngFor=\"let info of infos$\" style=\"text-align:center\">\n                                    <td><h3>{{info.camera_id}}</h3></td>\n                                    <td><h3>{{info.crowdCount}}</h3></td>\n                                    <td><h3>{{info.dwellTime}}</h3></td>\n                                    <td><h3>{{info.currentSession}}</h3></td>\n                                </tr>\n                            </tbody>\n                        </table>  -->\n                        <ng2-smart-table [settings]=\"settings\" [source]=\"source\"></ng2-smart-table>\n\n                    </nb-tab>\n                    </nb-tabset>\n                    </nb-card>\n\n        </div>\n  </div>\n</div>\n<div>\n  \n</div>\n\n<!-- <div id=\"div1\" style=\"border:5px solid black;width:500px;height:500px\"  on-mouseover='test()'>\n\n</div> -->\n\n<!-- <canvas #canvasEl width=400 height=400></canvas> -->\n<!-- \n        <div class=\"box\" (mouseenter)=\"onEvent($event)\"\n                         (mouseleave)=\"onEvent($event)\"\n                         (mousemove)=\"coordinates($event)\"\n                         on-click=\"onEvent($event)\"\n                         on-dblclick=\"onEvent($event)\"\n                         on-contextmenu=\"onEvent($event)\">\n            <p class=\"type\">Type: {{event?.type}}</p>\n            <p>x: {{clientX}}, y: {{clientY}}</p>\n            <p>ctrl: {{event?.ctrlKey}}, shift: {{event?.shiftKey}}, meta: {{event?.metaKey}}</p>\n        </div> -->"

/***/ }),

/***/ "./src/app/pages/group-a/analytic-a1/analytic-a1.component.scss":
/*!**********************************************************************!*\
  !*** ./src/app/pages/group-a/analytic-a1/analytic-a1.component.scss ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host::ng-deep#canvas {\n  border: 2px solid black;\n  position: relative;\n  width: 100%;\n  height: 450px; }\n\n:host::ng-deep#rectangle {\n  border: 5px dotted #FF0000;\n  position: absolute; }\n\n.box {\n  width: 500px;\n  height: 500px;\n  border: 1px solid LightGray;\n  padding: 10px; }\n\n.type {\n  font-size: 30px; }\n"

/***/ }),

/***/ "./src/app/pages/group-a/analytic-a1/analytic-a1.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/pages/group-a/analytic-a1/analytic-a1.component.ts ***!
  \********************************************************************/
/*! exports provided: AnalyticA1Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AnalyticA1Component", function() { return AnalyticA1Component; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../data.service */ "./src/app/data.service.ts");
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs-compat/_esm5/Rx.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AnalyticA1Component = /** @class */ (function () {
    //={
    //   camera_id: this.camera_id,
    //   left_length: this.left_length,
    //   top_length:this.top_length,
    //   width:this.width,
    //   height:this.height,
    //   text:this.text,
    // };
    function AnalyticA1Component(data) {
        this.data = data;
        this.drawingMode = false;
        this.deleteMode = false;
        this.perimeter = new Array();
        this.perimeter1 = new Array();
        this.complete = false;
        this.rectCoord$ = {};
        this.settings = {
            pager: {
                display: true,
                perPage: 4
            },
            columns: {
                camera_id: {
                    title: 'camera_id',
                    type: 'string',
                },
                crowdCount: {
                    title: 'Crowd Count',
                    type: 'number',
                },
                // crowdCountConfidence: {
                //   title: 'Crowd Count Confidence',
                //   type: 'number',
                // },
                dwellTime: {
                    title: 'Dwell Time',
                    type: 'number',
                },
                // dwellTimeConfidence: {
                //   title: 'Dwell Time Confidence',
                //   type: 'number',
                // },
                currentSession: {
                    title: 'Current Session',
                    type: 'number',
                },
                // currentSessionState: {
                //   title: 'Current Session State',
                //   type: 'number',
                // },
                totalUtilization: {
                    title: 'Total Utilization',
                    type: 'number',
                },
                peakUtilization: {
                    title: 'Peak Utilization',
                    type: 'number',
                },
                timestamp: {
                    title: 'Timestamp',
                    type: 'timestamp',
                    valuePrepareFunction: function (date) {
                        var raw = new Date(date);
                        var formatted = new _angular_common__WEBPACK_IMPORTED_MODULE_4__["DatePipe"]('en-EN').transform(raw, 'dd MMM yyyy HH:mm:ss');
                        return formatted;
                    }
                },
            },
            actions: false,
            attr: {
                class: 'table table-bordered',
            },
        };
        this.source = new ng2_smart_table__WEBPACK_IMPORTED_MODULE_3__["LocalDataSource"]();
        this.fillPolygon = function (pointsArray) {
            console.log("calledfill");
            // this.canvas= (this.canvasEl.nativeElement as HTMLCanvasElement);
            // this.ctx =this.canvas.getContext('2d');
            if (pointsArray.length <= 0)
                return;
            this.ctx.moveTo(pointsArray[0][0], pointsArray[0][1]);
            for (var i = 0; i < pointsArray.length; i++) {
                this.ctx.lineTo(pointsArray[i][0], pointsArray[i][1]);
            }
            this.ctx.closePath();
        };
        this.removepolygon = function (pointsArray) {
            this.ctx.globalCompositeOperation = 'destination-out';
            this.ctx.beginPath();
            if (pointsArray.length <= 0)
                return;
            this.ctx.moveTo(pointsArray[0][0], pointsArray[0][1]);
            for (var i = 0; i < pointsArray.length; i++) {
                this.ctx.lineTo(pointsArray[i][0], pointsArray[i][1]);
            }
            this.ctx.closePath();
            this.ctx.fill();
        };
        this.getCentroid = function (coord) {
            var center = coord.reduce(function (x, y) {
                return [x[0] + y[0] / coord.length, x[1] + y[1] / coord.length];
            }, [0, 0]);
            return center;
        };
        this.imgWidth = 760;
        this.imgHeight = 500;
        this.heatmap = '../assets/images/camera1.jpg';
    }
    AnalyticA1Component.prototype.ngOnInit = function () {
        var _this = this;
        this.getCamInfo();
        this.getInfo("cam0");
        this.timerSubscription = rxjs_Rx__WEBPACK_IMPORTED_MODULE_2__["Observable"].timer(1800).first().subscribe(function () { return _this.getInfo("cam0"); });
        // this.context = (this.canvasEl.nativeElement as HTMLCanvasElement).getContext('2d');    
        // this.initDraw(this.data,this.rectCoord$,"cam0");
        this.canvas = this.canvasEl.nativeElement;
        console.log(this.canvas);
        this.ctx = this.canvas.getContext('2d');
        this.element = this.img.nativeElement;
        // this.ctx.save();
        this.getRectCoords("cam0");
        console.log(this.rectCoord$);
        this.status = "none";
    };
    //  postRectCoords(x)
    //   {
    //     console.log(x);
    //     this.data.postRectCoords(x)
    //     .subscribe((status)=>
    //     {
    //       console.log(status);
    //     })
    //   }
    AnalyticA1Component.prototype.getCamInfo = function () {
        var _this = this;
        this.data.getCameras().subscribe(function (data) {
            _this.details$ = data;
        });
    };
    AnalyticA1Component.prototype.getInfo = function (x) {
        var _this = this;
        this.data.getHeatmapInfo(x).subscribe(function (info) {
            _this.timerSubscription.unsubscribe();
            _this.infos$ = info;
            _this.source.load(_this.infos$);
            // console.log(this.infos$[1]);
            _this.heatmap = String(_this.infos$[0].heatmap);
            // this.subscribeToData(x);
            _this.curr_cam = String(_this.infos$[0].camera_id);
            _this.timerSubscription = rxjs_Rx__WEBPACK_IMPORTED_MODULE_2__["Observable"].timer(1800).first().subscribe(function () { return _this.getInfo(x); });
        });
    };
    AnalyticA1Component.prototype.getRectCoords = function (x) {
        var _this = this;
        this.data.getRectCoords(x).subscribe(function (data) {
            _this.coords$ = data;
            // this.timerSubscription1 = Observable.timer(1800).first().subscribe(() => this.getRectCoords(x));
            // console.log(this.coords$);
        });
    };
    AnalyticA1Component.prototype.subscribeToData = function (x) {
        var _this = this;
        this.timerSubscription = rxjs_Rx__WEBPACK_IMPORTED_MODULE_2__["Observable"].timer(1800).first().subscribe(function () { return _this.getInfo(x); });
    };
    //  initDraw(data,rectCoord$,cam_id) :any {
    //   var canvas=(<HTMLInputElement>document.getElementById('canvas'));
    // console.log("called");
    //  function postRectCoords(x)
    //   {
    //     console.log(x);
    //     data.postRectCoords(x)
    //     .subscribe((status)=>
    //     {
    //       console.log(status);
    //     })
    //   }
    //   function setMousePosition(e) {
    //       var ev = e || window.event; //Moz || IE
    //       if (ev.pageX) { //Moz
    //           mouse.x = ev.pageX + window.pageXOffset;
    //           mouse.y = ev.pageY + window.pageYOffset;
    //       } else if (ev.clientX) { //IE
    //           mouse.x = ev.clientX + document.body.scrollLeft;
    //           mouse.y = ev.clientY + document.body.scrollTop;
    //       }
    //   };
    //   var mouse = {
    //       x: 0,
    //       y: 0,
    //       startX: 0,
    //       startY: 0
    //   };
    //   var element = null;
    //   var element1 = null;
    //   canvas.onmousemove = function (e) {
    //       setMousePosition(e);
    //       if (element !== null) {
    //         var x,y;
    //         var offset = $(this).offset();
    //         x = e.pageX- offset.left;
    //         y = e.pageY- offset.top;
    //           element.style.width = Math.abs(x - mouse.startX) + 'px';
    //           element.style.height = Math.abs(y - mouse.startY) + 'px';
    //           element.style.left = (x - mouse.startX < 0) ? x + 'px' : mouse.startX + 'px';
    //           element.style.top = (y - mouse.startY < 0) ? y + 'px' : mouse.startY + 'px';
    //       }
    //   }
    //   canvas.onclick = function (e) {
    //       if (element !== null) { 
    //   rectCoord$.camera_id=cam_id;
    //   rectCoord$.left_length=element.style.left;
    //   rectCoord$.top_length=element.style.top;
    //   rectCoord$.width=element.style.width;
    //   rectCoord$.height=element.style.height;
    //   rectCoord$.text=element1.innerHTML;
    //   // rectCoord$=new rectCoords();
    //   console.log(rectCoord$);
    //   postRectCoords(rectCoord$);
    //     // postRectCoords();
    //           element = null;
    //           canvas.style.cursor = "default";
    //           console.log("finsihed.");
    //       } else {
    //         var x,y;
    //         var offset = $(this).offset();
    //         x = e.pageX- offset.left;
    //         y = e.pageY- offset.top;
    //           console.log("begun.");
    //           console.log(mouse.x);
    //           mouse.startX = x;
    //           mouse.startY = y;
    //           element = document.createElement('div');
    //           element.id = 'rectangle'
    //           element.style.left = x + 'px';
    //           element.style.top = y + 'px';
    //           var data=prompt("Enter Name");
    //           element1=document.createElement('h1');
    //           element1.innerHTML=data;
    //           element1.style.textAlign="center";
    //           element.appendChild(element1);
    //           canvas.appendChild(element);
    //           canvas.style.cursor = "crosshair";
    //       }
    //   }
    // }
    // test(){
    // var x,y;
    //     $("#div1").mousemove(function(event) {
    //         var offset = $(this).offset();
    //         x = event.pageX- offset.left;
    //         y = event.pageY- offset.top;
    //         $("#div1").html("(X: "+x+", Y: "+y+")");
    //     });
    //   }
    // test(mouseEvent)
    // {
    //   alert("hello");
    //   var obj = document.getElementById("test");
    //   var obj_left = 0;
    //   var obj_top = 0;
    //   var xpos;
    //   var ypos;
    //   while (obj.offsetParent)
    //   {
    //     obj_left += obj.offsetLeft;
    //     obj_top += obj.offsetTop;
    //     obj = obj.offsetParent;
    //   }
    //   if (mouseEvent)
    //   {
    //     //FireFox
    //     xpos = mouseEvent.pageX;
    //     ypos = mouseEvent.pageY;
    //   }
    //   else
    //   {
    //     //IE
    //     xpos = window.event.x + document.body.scrollLeft - 2;
    //     ypos = window.event.y + document.body.scrollTop - 2;
    //   }
    //   xpos -= obj_left;
    //   ypos -= obj_top;
    //   document.getElementById("objectCoords").innerHTML = xpos + ", " + ypos;
    // }
    // private draw() {
    //   this.context.beginPath();
    //     this.context.moveTo(0,0);
    //     this.context.lineTo(300,150);
    //     this.context.stroke();
    // }
    // event: MouseEvent;
    // clientX = 0;
    // clientY = 0;
    // onEvent(event: MouseEvent): void {
    //     this.event = event;
    // }
    // coordinates(event: MouseEvent): void {
    //     this.clientX = event.clientX;
    //     this.clientY = event.clientY;
    // }
    AnalyticA1Component.prototype.postRectCoords = function (x) {
        console.log(x);
        this.data.postRectCoords(x)
            .subscribe(function (status) {
            console.log(status);
        });
    };
    AnalyticA1Component.prototype.afterLoading = function () {
        this.ctx.clearRect(0, 0, this.imgWidth, this.imgHeight);
        this.ctx.beginPath();
        console.log('drawImage');
        // this prints an image element with src I gave
        console.log(this.element);
        console.log("called afterLoading");
        this.ctx.drawImage(this.element, 0, 0, this.imgWidth, this.imgHeight);
        //  this.ctx.stroke();
        console.log(this.coords$);
        for (var i = 0; i < this.coords$.length; i++) {
            if (this.coords$[i].coordArray != "") {
                console.log(this.coords$[i].coordArray);
                this.fillPolygon(JSON.parse(this.coords$[i].coordArray));
                this.ctx.font = "14px Arial";
                this.ctx.fillStyle = "lightgreen";
                this.ctx.textAlign = 'center';
                this.ctx.fillText(this.coords$[i].text, this.getCentroid(JSON.parse(this.coords$[i].coordArray))[0], this.getCentroid(JSON.parse(this.coords$[i].coordArray))[1]);
                console.log("done");
            }
        }
        // this.ctx.clearRect(0, 0, this.imgWidth, this.imgHeight);
        //  this.renderPolygon();
        //  this.ctx.clearRect(0, 0, this.imgWidth, this.imgHeight);
        this.renderPolygon();
        //        var polygonPoints = [[395,174],[508,324],[257,317]];
        //        var polygonPoints1 = [[186,416.265625],[333,392.265625],[387,486.265625],[212,486.265625]]
        // this.fillPolygon(polygonPoints);
        // this.fillPolygon(polygonPoints1);
    };
    AnalyticA1Component.prototype.line_intersects = function (p0, p1, p2, p3) {
        var s1_x, s1_y, s2_x, s2_y;
        s1_x = p1['x'] - p0['x'];
        s1_y = p1['y'] - p0['y'];
        s2_x = p3['x'] - p2['x'];
        s2_y = p3['y'] - p2['y'];
        var s, t;
        s = (-s1_y * (p0['x'] - p2['x']) + s1_x * (p0['y'] - p2['y'])) / (-s2_x * s1_y + s1_x * s2_y);
        t = (s2_x * (p0['y'] - p2['y']) - s2_y * (p0['x'] - p2['x'])) / (-s2_x * s1_y + s1_x * s2_y);
        if (s >= 0 && s <= 1 && t >= 0 && t <= 1) {
            // Collision detected
            return true;
        }
        return false; // No collision
    };
    AnalyticA1Component.prototype.point = function (x, y) {
        console.log("called");
        this.ctx.fillStyle = "white";
        this.ctx.strokeStyle = "white";
        this.ctx.fillRect(x - 2, y - 2, 4, 4);
        this.ctx.moveTo(x, y);
    };
    AnalyticA1Component.prototype.undo = function () {
        //   this.canvas= (this.canvasEl.nativeElement as HTMLCanvasElement);
        //   this.canvas.style.cursor="auto";
        this.status = "undo";
        this.drawingMode = false;
        this.deleteMode = false;
        this.ctx.beginPath();
        // this.ctx = undefined
        this.perimeter.splice(0, this.perimeter.length);
        this.perimeter1.splice(0, this.perimeter1.length);
        // this.perimeter.pop();
        // this.perimeter1.pop();
        this.complete = false;
        this.start(true);
    };
    AnalyticA1Component.prototype.clear_canvas = function () {
        this.ctx.beginPath();
        this.perimeter = new Array();
        this.perimeter1 = new Array();
        this.complete = false;
        // document.getElementById('coordinates').value = '';
        this.start(true);
    };
    AnalyticA1Component.prototype.draw = function (end) {
        this.ctx.lineWidth = 1;
        this.ctx.strokeStyle = "white";
        this.ctx.lineCap = "square";
        this.ctx.beginPath();
        for (var i = 0; i < this.perimeter.length; i++) {
            if (i == 0) {
                this.ctx.moveTo(this.perimeter[i]['x'], this.perimeter[i]['y']);
                end || this.point(this.perimeter[i]['x'], this.perimeter[i]['y']);
            }
            else {
                this.ctx.lineTo(this.perimeter[i]['x'], this.perimeter[i]['y']);
                end || this.point(this.perimeter[i]['x'], this.perimeter[i]['y']);
            }
        }
        if (end) {
            this.text = prompt("Enter Name");
            if (this.text != "") {
                this.ctx.lineTo(this.perimeter[0]['x'], this.perimeter[0]['y']);
                this.ctx.closePath();
                this.ctx.fillStyle = 'rgba(255, 0, 0, 0.3)';
                this.ctx.fill();
                this.ctx.strokeStyle = 'blue';
                this.complete = true;
            }
            else {
                this.afterLoading();
            }
        }
        this.ctx.stroke();
        //print coordinates
        if (this.perimeter.length == 0) {
            console.log("0");
        }
        else {
            //   <HTMLElement>document.getElementById('coordinates').value = JSON.stringify(this.perimeter);
            console.log(this.perimeter1);
        }
    };
    AnalyticA1Component.prototype.check_intersect = function (x, y) {
        if (this.perimeter.length < 4) {
            return false;
        }
        var p0 = new Array();
        var p1 = new Array();
        var p2 = new Array();
        var p3 = new Array();
        p2['x'] = this.perimeter[this.perimeter.length - 1]['x'];
        p2['y'] = this.perimeter[this.perimeter.length - 1]['y'];
        p3['x'] = x;
        p3['y'] = y;
        for (var i = 0; i < this.perimeter.length - 1; i++) {
            p0['x'] = this.perimeter[i]['x'];
            p0['y'] = this.perimeter[i]['y'];
            p1['x'] = this.perimeter[i + 1]['x'];
            p1['y'] = this.perimeter[i + 1]['y'];
            if (p1['x'] == p2['x'] && p1['y'] == p2['y']) {
                continue;
            }
            if (p0['x'] == p3['x'] && p0['y'] == p3['y']) {
                continue;
            }
            if (this.line_intersects(p0, p1, p2, p3) == true) {
                return true;
            }
        }
        return false;
    };
    AnalyticA1Component.prototype.point_it = function (event) {
        //   if(this.complete){
        //       alert('Polygon already created');
        //        return false;
        //   }
        console.log(this.drawingMode);
        if (this.drawingMode == true) {
            var rect, x, y;
            if (event.ctrlKey || event.which === 3 || event.button === 2) {
                if (this.perimeter.length == 2) {
                    alert('You need at least three points for a polygon');
                    return false;
                }
                x = this.perimeter[0]['x'];
                y = this.perimeter[0]['y'];
                if (this.check_intersect(x, y)) {
                    alert('The line you are drowing intersect another line');
                    return false;
                }
                this.draw(true);
                console.log(this.text);
                if (this.text == "") {
                    alert("Name Cannot Be An Empty Field Please Try Again By Entering The Name");
                }
                this.ctx.font = "14px Arial";
                this.ctx.fillStyle = "lightgreen";
                this.ctx.textAlign = 'center';
                this.ctx.fillText(this.text, this.getCentroid(this.perimeter1)[0], this.getCentroid(this.perimeter1)[1]);
                console.log(this.getCentroid(this.perimeter1));
                this.rectCoord$.camera_id = this.curr_cam;
                this.rectCoord$.text = this.text;
                this.rectCoord$.coordArray = JSON.stringify(this.perimeter1);
                console.log(this.rectCoord$);
                if (this.text != "") {
                    this.postRectCoords(this.rectCoord$);
                    this.coords$.push(this.rectCoord$);
                }
                this.afterLoading();
                this.perimeter = new Array();
                this.perimeter1 = new Array();
                event.stopPropagation();
                event.preventDefault();
                alert('Polygon closed');
                return false;
            }
            else {
                rect = this.canvas.getBoundingClientRect();
                x = event.clientX - rect.left;
                y = event.clientY - rect.top;
                if (this.perimeter.length > 0 && x == this.perimeter[this.perimeter.length - 1]['x'] && y == this.perimeter[this.perimeter.length - 1]['y']) {
                    // same point - double click
                    return false;
                }
                if (this.check_intersect(x, y)) {
                    alert('The line you are drawing intersect another line');
                    return false;
                }
                this.perimeter.push({ 'x': x, 'y': y });
                this.perimeter1.push([x, y]);
                this.draw(false);
                return false;
            }
        }
    };
    AnalyticA1Component.prototype.start = function (with_draw) {
        this.canvas = this.canvasEl.nativeElement;
        this.ctx = this.canvas.getContext('2d');
        this.element = this.img.nativeElement;
        this.afterLoading();
    };
    AnalyticA1Component.prototype.alert1 = function () {
        alert("hello");
    };
    AnalyticA1Component.prototype.renderPolygon = function () {
        this.ctx.strokeStyle = "blue";
        this.ctx.fillStyle = "rgba(255,0,0,0.3)";
        this.ctx.setLineDash([5]);
        this.ctx.stroke();
        this.ctx.fill();
        console.log("calledrender");
    };
    AnalyticA1Component.prototype.IsInPath = function (event) {
        var rect, x, y;
        this.canvas = this.canvasEl.nativeElement;
        rect = this.canvas.getBoundingClientRect();
        x = (event.clientX - rect.left) * (this.canvas.width / rect.width);
        y = (event.clientY - rect.top) * (this.canvas.height / rect.height);
        this.locationX = x;
        this.locationY = y;
    };
    AnalyticA1Component.prototype.toggleEditor = function () {
        var r = confirm("Opening Editor Mode Do You Want To Continue ?");
        if (r == true) {
            var editor = document.getElementById("editor");
            editor.style.display = "flex";
            var status = document.getElementById("status");
            status.style.display = "block";
        }
    };
    AnalyticA1Component.prototype.closeEditor = function () {
        var r = confirm("Are You Sure About closing editor mode ?");
        if (r == true) {
            var editor = document.getElementById("editor");
            editor.style.display = "none";
            var status = document.getElementById("status");
            status.style.display = "none";
            this.drawingMode = false;
            this.canvas = this.canvasEl.nativeElement;
            this.canvas.style.cursor = "auto";
        }
        this.undo();
        this.status = "None";
    };
    AnalyticA1Component.prototype.drawOnCanvas = function () {
        this.status = "Draw";
        this.canvas = this.canvasEl.nativeElement;
        this.canvas.style.cursor = "crosshair";
        this.deleteMode = false;
        this.drawingMode = true;
    };
    AnalyticA1Component.prototype.deleteOnCanvas = function () {
        this.status = "Delete";
        this.canvas = this.canvasEl.nativeElement;
        this.canvas.style.cursor = "pointer";
        this.drawingMode = false;
        this.deleteMode = true;
    };
    // inside(point, vs) {
    //   var x = point[0], y = point[1];
    //   var inside = false;
    //   for (var i = 0, j = vs.length - 1; i < vs.length; j = i++) {
    //       var xi = vs[i][0], yi = vs[i][1];
    //       var xj = vs[j][0], yj = vs[j][1];
    //       var intersect = ((yi > y) != (yj > y))
    //           && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
    //       if (intersect) inside = !inside;
    //   }
    //   return inside;
    // }
    AnalyticA1Component.prototype.delete = function () {
        if (this.deleteMode) {
            console.log(this.locationX);
            console.log(this.locationY);
            var x = this.locationX, y = this.locationY;
            for (var k = 0; k < this.coords$.length; k++) {
                if (this.coords$[k].coordArray != "") {
                    var vs = JSON.parse(this.coords$[k].coordArray);
                    var inside = false;
                    for (var i = 0, j = vs.length - 1; i < vs.length; j = i++) {
                        var xi = vs[i][0], yi = vs[i][1];
                        var xj = vs[j][0], yj = vs[j][1];
                        var intersect = ((yi > y) != (yj > y))
                            && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
                        if (intersect)
                            inside = !inside;
                    }
                    if (inside == true) {
                        // this.removepolygon(JSON.parse(this.coords$[k].coordArray));
                        console.log(JSON.parse(this.coords$[k].coordArray));
                        console.log(this.coords$[k].camera_id);
                        console.log(this.coords$[k].text);
                        this.deleteCoords(this.coords$[k].camera_id, this.coords$[k].text);
                        this.coords$.splice(k, 1);
                        console.log(this.coords$);
                        this.afterLoading();
                    }
                }
            }
        }
    };
    AnalyticA1Component.prototype.deleteCoords = function (x, y) {
        this.data.deletePost(x, y)
            .subscribe(function (status) {
            console.log(status);
        });
        alert("Successfully deleted");
    };
    AnalyticA1Component.prototype.sleep = function (milliseconds) {
        var start = new Date().getTime();
        for (var i = 0; i < 1e7; i++) {
            if ((new Date().getTime() - start) > milliseconds) {
                break;
            }
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('canvasEl'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], AnalyticA1Component.prototype, "canvasEl", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('img'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], AnalyticA1Component.prototype, "img", void 0);
    AnalyticA1Component = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'analytic-a1',
            template: __webpack_require__(/*! ./analytic-a1.component.html */ "./src/app/pages/group-a/analytic-a1/analytic-a1.component.html"),
            styles: [__webpack_require__(/*! ./analytic-a1.component.scss */ "./src/app/pages/group-a/analytic-a1/analytic-a1.component.scss")]
        }),
        __metadata("design:paramtypes", [_data_service__WEBPACK_IMPORTED_MODULE_1__["DataService"]])
    ], AnalyticA1Component);
    return AnalyticA1Component;
}());



/***/ }),

/***/ "./src/app/pages/group-a/analytic-a2/analytic-a2.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/pages/group-a/analytic-a2/analytic-a2.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n  <div class=\"col-xxxl-3 col-xxl-3 col-md-8\">\n    <div class=\"row\">\n        <div class=\"col-xxxl-12 col-xxl-12 col-md-8\">\n            <nb-card >\n              <nb-tabset fullWidth>\n                <nb-tab tabTitle=\"Gender\">\n                    <h1 style=\"text-align:center;padding-top:5%;font-size:50px;color:lightgreen\">Male</h1>\n                </nb-tab>\n                </nb-tabset>\n                </nb-card>\n          </div>\n    </div>\n    <div class=\"row\">\n        <div class=\"col-xxxl-12 col-xxl-12 col-md-8\">\n            <nb-card>\n              <nb-tabset fullWidth>\n                <nb-tab tabTitle=\"Age\">\n                    <h1 style=\"text-align:center;padding-top:5%;font-size:50px;color:lightgreen\">25</h1>\n                </nb-tab>\n                </nb-tabset>\n                </nb-card>\n          </div>\n    </div>\n    <div class=\"row\">\n        <div class=\"col-xxxl-12 col-xxl-12 col-md-8\">\n            <nb-card>\n              <nb-tabset fullWidth>\n                <nb-tab tabTitle=\"test\">    \n                    <h1 style=\"text-align:center;padding-top:5%;font-size:50px;color:lightgreen\">Hello</h1>\n                </nb-tab>\n                </nb-tabset>\n                </nb-card>\n          </div>\n    </div>\n  </div>\n \n  <div class=\"col-xxxl-6 col-xxl-6 col-md-8\">\n      <nb-card size=\"medium\">\n          <nb-tabset fullWidth>\n            <nb-tab tabTitle=\"Image\">\n                <!-- <h1 style=\"text-align:center;padding-top:5%;font-size:50px;color:lightgreen\">Hello</h1> -->\n                <img src=\"assets/images/camera1.jpg\" style=\"width:100%;height:400px;margin:0 auto\">\n            </nb-tab>\n            </nb-tabset>\n      </nb-card>\n  </div>\n\n  <div class=\"col-xxxl-3 col-xxl-3 col-md-8\">\n      <div class=\"row\">\n          <div class=\"col-xxxl-12 col-xxl-12 col-md-8\">\n              <nb-card>\n                <nb-tabset fullWidth>\n                  <nb-tab tabTitle=\"test\">\n                      <h1 style=\"text-align:center;padding-top:5%;font-size:50px;color:lightgreen\">Hello</h1>\n                  </nb-tab>\n                  </nb-tabset>\n                  </nb-card>\n            </div>\n      </div>\n      <div class=\"row\">\n          <div class=\"col-xxxl-12 col-xxl-12 col-md-8\">\n              <nb-card>\n                <nb-tabset fullWidth>\n                  <nb-tab tabTitle=\"test\">\n                      <h1 style=\"text-align:center;padding-top:5%;font-size:50px;color:lightgreen\">Hello</h1>\n                  </nb-tab>\n                  </nb-tabset>\n                  </nb-card>\n            </div>\n      </div>\n      <div class=\"row\">\n          <div class=\"col-xxxl-12 col-xxl-12 col-md-8\">\n              <nb-card>\n                <nb-tabset fullWidth>\n                  <nb-tab tabTitle=\"test\">\n                      <h1 style=\"text-align:center;padding-top:5%;font-size:50px;color:lightgreen\">Hello</h1>\n                  </nb-tab>\n                  </nb-tabset>\n                  </nb-card>\n            </div>\n      </div>\n    </div>\n</div>\n\n<div class=\"row\">\n    <div class=\"col-xxxl-12 col-xxl-12 col-md-12\">\n  <nb-card size=\"medium\">\n    <nb-card-header>\n     <h2 style=\"float: left;\"> Log Table</h2>\n      <form #f=\"ngForm\" (ngSubmit)=\"onSubmit(f)\" style=\"float:right\">\n      <input type=\"date\" name=\"date\" style=\" border-radius: 25px;border: 2px solid #609;\" ngModel required>\n      <button type=\"submit\" class=\"btn btn-success\">Submit</button>\n      </form>\n    </nb-card-header>\n  \n    <nb-card-body>\n            <div><ng2-smart-table [settings]=\"settings\" [source]=\"source\" (userRowSelect)=\"onUserRowSelect($event)\" ></ng2-smart-table></div>\n\n    </nb-card-body>\n  </nb-card> \n\n  </div>\n  \n</div>"

/***/ }),

/***/ "./src/app/pages/group-a/analytic-a2/analytic-a2.component.scss":
/*!**********************************************************************!*\
  !*** ./src/app/pages/group-a/analytic-a2/analytic-a2.component.scss ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host /deep/ ng2-smart-table table {\n  width: 100%;\n  display: block;\n  border: 2px solid darkblue; }\n\n:host /deep/ ng2-smart-table tbody {\n  height: 200px;\n  display: inline-block;\n  width: 100%;\n  overflow: auto; }\n\n:host /deep/ ng2-smart-table tbody td {\n  width: 140px; }\n"

/***/ }),

/***/ "./src/app/pages/group-a/analytic-a2/analytic-a2.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/pages/group-a/analytic-a2/analytic-a2.component.ts ***!
  \********************************************************************/
/*! exports provided: AnalyticA2Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AnalyticA2Component", function() { return AnalyticA2Component; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../data.service */ "./src/app/data.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AnalyticA2Component = /** @class */ (function () {
    function AnalyticA2Component(data) {
        this.data = data;
        this.infos_new$ = [];
        this.settings = {
            columns: {
                heatmap: {
                    title: 'heatmap',
                    type: 'html',
                    valuePrepareFunction: function (picture) { return "<img width=\"100px\" height=\"100px\" src=\"" + picture + "\" />"; }
                },
                camera_id: {
                    title: 'camera_id',
                    type: 'string',
                },
                crowdCount: {
                    title: 'Crowd Count',
                    type: 'number',
                },
                crowdCountConfidence: {
                    title: 'Crowd Count Confidence',
                    type: 'number',
                },
                dwellTime: {
                    title: 'Dwell Time',
                    type: 'number',
                },
                dwellTimeConfidence: {
                    title: 'Dwell Time Confidence',
                    type: 'number',
                },
                currentSession: {
                    title: 'Current Session',
                    type: 'number',
                },
                currentSessionState: {
                    title: 'Current Session State',
                    type: 'number',
                },
                totalUtilization: {
                    title: 'Total Utilization',
                    type: 'number',
                },
                peakUtilization: {
                    title: 'Peak Utilization',
                    type: 'number',
                },
                timestamp: {
                    title: 'Timestamp',
                    type: 'timestamp',
                    valuePrepareFunction: function (date) {
                        var raw = new Date(date);
                        var formatted = new _angular_common__WEBPACK_IMPORTED_MODULE_2__["DatePipe"]('en-EN').transform(raw, 'dd MMM yyyy HH:mm:ss');
                        return formatted;
                    }
                },
            },
            actions: false,
            attr: {
                class: 'table table-bordered',
            },
        };
        this.source = new ng2_smart_table__WEBPACK_IMPORTED_MODULE_1__["LocalDataSource"]();
    }
    AnalyticA2Component.prototype.ngOnInit = function () {
        this.heatmap = "assets/images/camera3.jpg";
        this.getInfo("cam0");
        this.getCamInfo();
    };
    AnalyticA2Component.prototype.getInfo = function (x) {
        var _this = this;
        this.data.getHeatmapInfo(x).subscribe(function (info) {
            _this.infos$ = info;
            _this.source.load(_this.infos$);
            console.log(_this.infos$);
            // this.subscribeToData(x);
        });
    };
    AnalyticA2Component.prototype.onUserRowSelect = function (event) {
        console.log(event.data.heatmap);
        this.heatmap = event.data.heatmap;
        this.x = event.data.camera_id;
    };
    AnalyticA2Component.prototype.getCamInfo = function () {
        var _this = this;
        this.data.getCameras().subscribe(function (data) {
            _this.details$ = data;
        });
    };
    AnalyticA2Component.prototype.formatDate = function (timestamp) {
        timestamp = new Date(timestamp);
        var d = timestamp.getDate().toString();
        var dd = (d.length === 2) ? d : "0" + d;
        var m = (timestamp.getMonth() + 1).toString();
        var mm = (m.length === 2) ? m : "0" + m;
        return ((timestamp.getFullYear()).toString() + "-" + mm + "-" + dd);
    };
    AnalyticA2Component.prototype.onSubmit = function (f) {
        this.infos_new$ = new Array();
        for (var i = 0; i < this.infos$.length; i++) {
            var m = this.formatDate(this.infos$[i].timestamp);
            if (f.value.date == m) {
                this.infos_new$.push(this.infos$[i]);
            }
        }
        console.log(this.infos_new$);
        this.source.load(this.infos_new$);
    };
    AnalyticA2Component = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'analytic-a2',
            template: __webpack_require__(/*! ./analytic-a2.component.html */ "./src/app/pages/group-a/analytic-a2/analytic-a2.component.html"),
            styles: [__webpack_require__(/*! ./analytic-a2.component.scss */ "./src/app/pages/group-a/analytic-a2/analytic-a2.component.scss")]
        }),
        __metadata("design:paramtypes", [_data_service__WEBPACK_IMPORTED_MODULE_3__["DataService"]])
    ], AnalyticA2Component);
    return AnalyticA2Component;
}());



/***/ }),

/***/ "./src/app/pages/group-a/analytic-a3/analytic-a3.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/pages/group-a/analytic-a3/analytic-a3.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n    <div class=\"col-xxxl-12 col-md-8\">\n      <nb-card size=\"xxsmall\">\n        <h1 style=\"text-align:center;padding-top:1.5%;padding-bottom:2.5%\">Smart Poster Analytics</h1>\n      </nb-card>\n    </div>\n  \n</div>\n  \n<div class=\"row\">\n    <div class=\"col-xxxl-4 col-xxl-4 col-md-8\">\n        <nb-card size=\"xlarge\">\n            <h1 style=\"text-align:center;padding-top:10%;color:lightgreen\">Add poster</h1>\n            <br>\n            <br>\n            <br>\n            <img src=\"assets/images/add2.png\"  style=\"width:400px;height:400px;margin-left:auto;margin-right:auto\">\n            <br>\n            <br>\n            <img src=\"assets/images/arrow.png\"  style=\"width:100px;height:100px;margin-left:auto;margin-right:auto\" routerLink=\"/pages/addInfo\">\n            \n   \n    </nb-card>\n    </div>\n    <div class=\"col-xxxl-4 col-xxl-4 col-md-8\">\n        <nb-card size=\"xlarge\">\n            <h1 style=\"text-align:center;padding-top:10%;color:lightgreen\">Dashboard</h1>\n            <br>\n            <br>\n            <br>\n            <img src=\"assets/images/dashboard.png\"  style=\"width:550px;height:400px;margin-left:auto;margin-right: auto\">\n            <br>\n            <br>\n            <img src=\"assets/images/arrow.png\"  style=\"width:100px;height:100px;margin-left:auto;margin-right: auto\" routerLink=\"/pages/log\">\n    </nb-card>\n    </div>\n    <div class=\"col-xxxl-4 col-xxl-4 col-md-8\">\n        <nb-card size=\"xlarge\">\n            <h1 style=\"text-align:center;padding-top:10%;color:lightgreen\" >View Poster</h1>\n            <br>\n            <br>\n            <br>\n            <img src=\"assets/images/poster.png\"  style=\"width:550px;height:400px;margin-left:auto;margin-right:auto\">\n            <br>\n            <br>\n            <img src=\"assets/images/arrow.png\"  style=\"width:100px;height:100px;margin-left:auto;margin-right:auto\"  routerLink=\"/intro\">\n\n    </nb-card>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/pages/group-a/analytic-a3/analytic-a3.component.scss":
/*!**********************************************************************!*\
  !*** ./src/app/pages/group-a/analytic-a3/analytic-a3.component.scss ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/group-a/analytic-a3/analytic-a3.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/pages/group-a/analytic-a3/analytic-a3.component.ts ***!
  \********************************************************************/
/*! exports provided: AnalyticA3Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AnalyticA3Component", function() { return AnalyticA3Component; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../data.service */ "./src/app/data.service.ts");
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs-compat/_esm5/Rx.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AnalyticA3Component = /** @class */ (function () {
    function AnalyticA3Component(data) {
        this.data = data;
        this.gaugeType = "arch";
        this.thresholdConfig = {
            '0': { color: 'green' },
            '40': { color: 'orange' },
            '75.5': { color: 'red' }
        };
        this.gaugeLabel = "Water-Pressure";
        this.gaugeAppendText = "Pa";
        this.editableText = 'myText';
        this.editablePassword = 'myPassword';
        this.editableTextArea = 'Text in text area';
        this.editableSelect = 2;
        this.editableSelectOptions = [
            { value: 1, text: 'status1' },
            { value: 2, text: 'status2' },
            { value: 3, text: 'status3' },
            { value: 4, text: 'status4' }
        ];
    }
    AnalyticA3Component.prototype.saveEditable = function (value) {
        //call to http service
        console.log('http.service: ' + value);
    };
    AnalyticA3Component.prototype.ngOnInit = function () {
        var _this = this;
        this.timerSubscription = rxjs_Rx__WEBPACK_IMPORTED_MODULE_2__["Observable"].timer(1800).first().subscribe(function () { return _this.getInfo(); });
        this.gaugeValue1 = this.infos$[0].meter1;
        this.gaugeValue2 = this.infos$[0].meter2;
        this.gaugeValue3 = this.infos$[0].meter3;
        this.gaugeValue4 = this.infos$[0].meter4;
    };
    AnalyticA3Component.prototype.getInfo = function () {
        var _this = this;
        this.data.getmeterInfo().subscribe(function (info) {
            _this.infos$ = info;
            _this.gaugeValue1 = _this.infos$[0].meter1;
            _this.gaugeValue2 = _this.infos$[0].meter2;
            _this.gaugeValue3 = _this.infos$[0].meter3;
            _this.gaugeValue4 = _this.infos$[0].meter4;
            console.log(_this.gaugeValue1);
            console.log(_this.gaugeValue2);
            console.log(_this.gaugeValue3);
            console.log(_this.gaugeValue4);
            _this.timerSubscription = rxjs_Rx__WEBPACK_IMPORTED_MODULE_2__["Observable"].timer(1800).first().subscribe(function () { return _this.getInfo(); });
        });
    };
    AnalyticA3Component = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'analytic-a3',
            template: __webpack_require__(/*! ./analytic-a3.component.html */ "./src/app/pages/group-a/analytic-a3/analytic-a3.component.html"),
            styles: [__webpack_require__(/*! ./analytic-a3.component.scss */ "./src/app/pages/group-a/analytic-a3/analytic-a3.component.scss")]
        }),
        __metadata("design:paramtypes", [_data_service__WEBPACK_IMPORTED_MODULE_1__["DataService"]])
    ], AnalyticA3Component);
    return AnalyticA3Component;
}());



/***/ }),

/***/ "./src/app/pages/group-a/group-a.component.html":
/*!******************************************************!*\
  !*** ./src/app/pages/group-a/group-a.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1>\n  group-a works!\n</h1>\n"

/***/ }),

/***/ "./src/app/pages/group-a/group-a.component.scss":
/*!******************************************************!*\
  !*** ./src/app/pages/group-a/group-a.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/group-a/group-a.component.ts":
/*!****************************************************!*\
  !*** ./src/app/pages/group-a/group-a.component.ts ***!
  \****************************************************/
/*! exports provided: GroupAComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupAComponent", function() { return GroupAComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var GroupAComponent = /** @class */ (function () {
    function GroupAComponent() {
    }
    GroupAComponent.prototype.ngOnInit = function () {
    };
    GroupAComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'group-a',
            template: __webpack_require__(/*! ./group-a.component.html */ "./src/app/pages/group-a/group-a.component.html"),
            styles: [__webpack_require__(/*! ./group-a.component.scss */ "./src/app/pages/group-a/group-a.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], GroupAComponent);
    return GroupAComponent;
}());



/***/ }),

/***/ "./src/app/pages/group-a/group-a.module.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/group-a/group-a.module.ts ***!
  \*************************************************/
/*! exports provided: GroupAModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupAModule", function() { return GroupAModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _theme_theme_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../@theme/theme.module */ "./src/app/@theme/theme.module.ts");
/* harmony import */ var _group_a_routing__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./group-a.routing */ "./src/app/pages/group-a/group-a.routing.ts");
/* harmony import */ var _group_a_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./group-a.component */ "./src/app/pages/group-a/group-a.component.ts");
/* harmony import */ var _analytic_a1_analytic_a1_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./analytic-a1/analytic-a1.component */ "./src/app/pages/group-a/analytic-a1/analytic-a1.component.ts");
/* harmony import */ var _analytic_a2_analytic_a2_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./analytic-a2/analytic-a2.component */ "./src/app/pages/group-a/analytic-a2/analytic-a2.component.ts");
/* harmony import */ var _analytic_a3_analytic_a3_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./analytic-a3/analytic-a3.component */ "./src/app/pages/group-a/analytic-a3/analytic-a3.component.ts");
/* harmony import */ var _dashboard_dashboard_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../dashboard/dashboard.module */ "./src/app/pages/dashboard/dashboard.module.ts");
/* harmony import */ var ng2_smart_table__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ng2-smart-table */ "./node_modules/ng2-smart-table/index.js");
/* harmony import */ var ngx_gauge__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ngx-gauge */ "./node_modules/ngx-gauge/ngx-gauge.es5.js");
/* harmony import */ var _qontu_ngx_inline_editor__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @qontu/ngx-inline-editor */ "./node_modules/@qontu/ngx-inline-editor/ngx-inline-editor.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













var GroupAModule = /** @class */ (function () {
    function GroupAModule() {
    }
    GroupAModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _group_a_routing__WEBPACK_IMPORTED_MODULE_4__["routing"],
                ng2_smart_table__WEBPACK_IMPORTED_MODULE_10__["Ng2SmartTableModule"],
                _theme_theme_module__WEBPACK_IMPORTED_MODULE_3__["ThemeModule"],
                _dashboard_dashboard_module__WEBPACK_IMPORTED_MODULE_9__["DashboardModule"],
                ngx_gauge__WEBPACK_IMPORTED_MODULE_11__["NgxGaugeModule"],
                _qontu_ngx_inline_editor__WEBPACK_IMPORTED_MODULE_12__["InlineEditorModule"]
            ],
            declarations: [
                _group_a_component__WEBPACK_IMPORTED_MODULE_5__["GroupAComponent"],
                _analytic_a1_analytic_a1_component__WEBPACK_IMPORTED_MODULE_6__["AnalyticA1Component"],
                _analytic_a2_analytic_a2_component__WEBPACK_IMPORTED_MODULE_7__["AnalyticA2Component"],
                _analytic_a3_analytic_a3_component__WEBPACK_IMPORTED_MODULE_8__["AnalyticA3Component"],
            ]
        })
    ], GroupAModule);
    return GroupAModule;
}());



/***/ }),

/***/ "./src/app/pages/group-a/group-a.routing.ts":
/*!**************************************************!*\
  !*** ./src/app/pages/group-a/group-a.routing.ts ***!
  \**************************************************/
/*! exports provided: routing */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routing", function() { return routing; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _group_a_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./group-a.component */ "./src/app/pages/group-a/group-a.component.ts");
/* harmony import */ var _analytic_a1_analytic_a1_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./analytic-a1/analytic-a1.component */ "./src/app/pages/group-a/analytic-a1/analytic-a1.component.ts");
/* harmony import */ var _analytic_a2_analytic_a2_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./analytic-a2/analytic-a2.component */ "./src/app/pages/group-a/analytic-a2/analytic-a2.component.ts");
/* harmony import */ var _analytic_a3_analytic_a3_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./analytic-a3/analytic-a3.component */ "./src/app/pages/group-a/analytic-a3/analytic-a3.component.ts");





var routes = [
    {
        path: '',
        component: _group_a_component__WEBPACK_IMPORTED_MODULE_1__["GroupAComponent"]
    },
    {
        path: 'analytic-a1',
        component: _analytic_a1_analytic_a1_component__WEBPACK_IMPORTED_MODULE_2__["AnalyticA1Component"]
    },
    {
        path: 'analytic-a2',
        component: _analytic_a2_analytic_a2_component__WEBPACK_IMPORTED_MODULE_3__["AnalyticA2Component"]
    },
    {
        path: 'analytic-a3',
        component: _analytic_a3_analytic_a3_component__WEBPACK_IMPORTED_MODULE_4__["AnalyticA3Component"]
    },
];
var routing = _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forChild(routes);


/***/ })

}]);
//# sourceMappingURL=group-a-group-a-module.js.map